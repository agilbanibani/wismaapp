import React from 'react'
import {Modal, View, ScrollView, Text, TextInput, TouchableOpacity} from 'react-native'
import DatePicker from 'react-native-datepicker'
import Colors from './src/style/Colors'
import {Icon} from 'react-native-elements'
import moment from 'moment'

const ModalAdd = ({show, onClose, submit, state, setState}) => {
   return(
      <Modal visible={show} onRequestClose={onClose}>
         <View style={{flex: 1, backgroundColor: '#fff'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
               <View style={{marginHorizontal: 10}}>
                  <View style={{marginTop: 20}}>
                     <Text>Tempat Acara</Text>
                     <TextInput
                        placeholder="Masukkan lokasi acara"
                        value={state.title}
                        onChangeText={(text) => setState('title', text)}
                        style={{width: '100%', height: 45, marginTop: 15, paddingHorizontal: 5, borderRadius: 7, borderWidth: 0.4, borderColor: 'grey'}}
                     />
                  </View>
                  <View style={{width: '100%', marginTop: 20}}>
                     <Text style={{color: "#000"}}>Tanggal Acara</Text>
                     <View style={{width: '100%', flexDirection: 'row', marginTop: 10, alignItems: 'center', paddingBottom: 5, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}>
                        {/* <TouchableOpacity style={{width: '90%', alignItems: 'flex-start'}}> */}
                           <DatePicker
                              style={{width: '90%'}}
                              date={state.tanggal}
                              mode="date"
                              placeholder="dd/mm/yyyy"
                              format="YYYY-MM-DD"
                              minDate={moment(new Date()).add('days', 1)}
                              // maxDate="2018-01-01"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              showIcon={false}
                              customStyles={{
                                 dateIcon: {
                                    flex: 0,
                                    width: 0,
                                    height: 0,
                                 },
                                 dateInput: {
                                    borderWidth: 0,
                                    alignItems: 'flex-start',
                                 }
                                 // ... You can check the source to find the other keys.
                              }}
                              onDateChange={(date) => setState('tanggal', date)}
                           />
                        {/* </TouchableOpacity> */}
                        <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                           <Icon
                              type="antdesign"
                              name="calendar"
                              size={20}
                              color={Colors.text_black}
                           />
                        </View>
                     </View>
                  </View>
                  <View style={{marginTop: 20}}>
                     <Text>Mulai dari Jam</Text>
                     <TextInput
                        placeholder="Masukkan jam dimulai acara"
                        keyboardType="number-pad"
                        value={state.jam_start}
                        onChangeText={(text) => setState('jam_start', text)}
                        style={{width: '100%', height: 45, marginTop: 15, paddingHorizontal: 5, borderRadius: 7, borderWidth: 0.4, borderColor: 'grey'}}
                     />
                  </View>
                  <View style={{marginTop: 20}}>
                     <Text>Sampai Jam</Text>
                     <TextInput
                        placeholder="Masukkan jam selesai acara"
                        keyboardType="number-pad"
                        value={state.jam_end}
                        onChangeText={(text) => setState('jam_end', text)}
                        style={{width: '100%', height: 45, marginTop: 15, paddingHorizontal: 5, borderRadius: 7, borderWidth: 0.4, borderColor: 'grey'}}
                     />
                  </View>
                  <View style={{marginTop: 20}}>
                     <Text>Catatan (jika ada)</Text>
                     <TextInput
                        placeholder="Masukkan catatan"
                        multiline
                        numberOfLines={10}
                        value={state.notes}
                        onChangeText={(text) => setState('notes', text)}
                        style={{width: '100%', height: 140, textAlignVertical: 'top', marginTop: 15, paddingHorizontal: 5, borderRadius: 7, borderWidth: 0.4, borderColor: 'grey'}}
                     />
                  </View>
                  <TouchableOpacity
                     activeOpacity={0.8}
                     onPress={submit}
                     style={{width: '100%', marginTop: 30, paddingVertical: 15, backgroundColor: 'blue', justifyContent: 'center', alignItems: 'center', borderRadius: 7}}
                  >
                     <Text style={{color: '#fff', fontSize: 18, fontWeight: 'bold'}}>Tambahkan Data</Text>
                  </TouchableOpacity>
               </View>
            </ScrollView>
         </View>
      </Modal>
   )
}

export default ModalAdd;