import React, {useState} from 'react'
import {
   View,
   Text,
   ScrollView,
   Image,
   TouchableOpacity,
   TextInput,
   StyleSheet
} from 'react-native'
import {Icon} from 'react-native-elements'
import ModalAdd from './ModalAddSchedule'
import moment from 'moment'

const Reminder = () => {

   const [showModal, setShowModal] = useState(false);
   const [state, setState] = useState({
      title: '',
      tanggal: moment(new Date()).format('YYYY-MM-DD'),
      jam_start: '',
      jam_end: '',
      notes: ''
   })
   const [list, setList] = useState([
      {
         title: 'Acara di bantul',
         tanggal: '10-01-2021',
         jam_start: '12:00',
         jam_end: '13.30',
         notes: ''
      }
   ])

   const addSchedule = () => {
      const {jam_end, title, tanggal, notes, jam_start} = state;
      var data = {
         title: title,
         tanggal: tanggal,
         jam_start: jam_start,
         jam_end: jam_end,
         notes: notes
      }
      list.push(data);
   }

   return(
      <View style={{flex: 1}}>
         <ModalAdd
            show={showModal}
            onClose={() => setShowModal(false)}
            state={state}
            setState={(key, val) => setState({...state, [key]:val})}
            submit={() => {
               setShowModal(false);
               addSchedule();
            }}
         />
         <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{width: '100%', paddingVertical: 15, justifyContent: 'center', alignItems: 'center'}}>
               <Text style={{fontWeight: '600', fontSize: 20}}>Reminder Schedule</Text>
            </View>
            {
               list.map(value => {
                  return(
                     <View style={S.card}>
                        <Text style={{fontWeight: 'bold', fontSize: 18}}>{value.title}</Text>
                        <Text style={{marginTop: 10}}>tanggal : <Text style={{fontWeight: 'bold', fontSize: 16}}>{value.tanggal}</Text></Text>
                        <Text style={{marginTop: 10}}>jam : <Text style={{fontWeight: 'bold', fontSize: 16}}>{value.jam_start} - {value.jam_end}</Text></Text>
                     </View>
                  )
               })
            }
         </ScrollView>
         <TouchableOpacity 
         activeOpacity={0.8}
         onPress={() => setShowModal(true)}
         style={{
            position: 'absolute',
            bottom: 10,
            right: 10,
            width: 50,
            height: 50,
            borderRadius: 25,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: 'red'
         }}>
            <Icon
               type="antdesign"
               name="plus"
               size={25}
               color="#fff"
            />
         </TouchableOpacity>
      </View>
   )
}

const S = StyleSheet.create({
   card: {
      backgroundColor: '#fff',
      shadowColor: '#413DAD',
      shadowOffset: {
         width: 0,
         height: 3,
      },
      shadowOpacity: 0.29,
      shadowRadius: 4.65,
      elevation: 4,
      width: '90%', 
      paddingHorizontal: 10, 
      alignSelf: 'center', 
      marginVertical: 15, 
      paddingVertical: 10, 
      borderRadius: 8,
   }
})

export default Reminder;