import React from 'react'
import {
   TouchableOpacity
} from 'react-native'

const Button = ({
   onPress,
   children,
   style
}) => {
   return (
      <TouchableOpacity
         activeOpacity={0.8}
         style={style}
         onPress={onPress}
      >
         {children}
      </TouchableOpacity>
   )
}
export default Button;