import React from 'react'
import {StyleSheet, Dimensions, TouchableOpacity, ImageBackground, ActivityIndicator, Text, View} from 'react-native'
import Carousel, { Pagination, ParallaxImage  } from 'react-native-snap-carousel';
import { Mixins } from '../style';
import Colors from '../style/Colors';

const { width: screenWidth } = Dimensions.get('window')
const Slider = ({data, loading, activeDotIndex, setState, onPress}) => {
   const _renderItem = ( {item,index}, parallaxProps) => {
      return (
         <TouchableOpacity 
            activeOpacity={0.7}
            onPress={() => onPress(item)}
            style={styles.item}>
            <ImageBackground
               resizeMode='cover'
               imageStyle={{borderRadius: 8}}
               style={{width: '100%', height: 200, marginTop: 20 }}
               source={{uri: item.image}}
            >
               <View style={{width: '100%', height: 120, backgroundColor: 'rgba(22, 35, 62, 0.3)'}}/>
               
               <View style={{width: '100%', height: 80, backgroundColor: 'rgba(22, 35, 62, 0.5)', justifyContent: 'space-between', paddingVertical: 10, paddingHorizontal: 5}}>
                  <Text style={{color: Colors.white, fontSize: Mixins.scaleFont(16), fontWeight: 'bold'}}>{item.name}</Text>
                  <Text numberOfLines={2} ellipsizeMode="tail" style={{color: Colors.white, fontSize: Mixins.scaleFont(20), fontWeight: 'bold'}}>{item.description}</Text>
               </View>
            </ImageBackground>
         </TouchableOpacity>
      );
   }

   if(loading) {
      return (
         <View style={{width: '100%', height: 150, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator color="red" size="large" />
         </View>
      )
   } else {
      return(
         <View>
            <Carousel
               onSnapToItem={index => setState(index)}
               sliderWidth={screenWidth}
               itemWidth={screenWidth - 60}
               data={data}
               renderItem={_renderItem}
            />
            <Pagination
               dotsLength={data.length}
               activeDotIndex={activeDotIndex}
               containerStyle={{paddingVertical: 8, alignSelf: 'flex-start'}}
               dotColor={'#90761C'}
               dotStyle={styles.paginationDot}
               inactiveDotColor={'#DADADA'}
               inactiveDotOpacity={1}
               inactiveDotScale={1}
            />
         </View>
      )
   }
}
const styles = StyleSheet.create({
   paginationDot: {
      width: 8,
      height: 8,
      borderRadius: 5,
      marginTop: 15,
      marginHorizontal: -5,
   },
   item: {
      width: Dimensions.get('window').width - 60,
    },
    imageContainer: {
      width: 300,
      height: 230,
      marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
      backgroundColor: 'white',
      borderRadius: 8,
    },
    image: {
      ...StyleSheet.absoluteFillObject,
      resizeMode: 'cover',
    },
})

export default Slider;