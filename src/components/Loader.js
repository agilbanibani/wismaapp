import React from 'react';
import {
  View,
  Modal,
  ActivityIndicator,
  Platform,
  SafeAreaView,
  StyleSheet
} from 'react-native';
import Colors from '../style/Colors';

const onIOS = Platform.OS === 'ios';

const Loader = ({show, type}) => {
  return (
    <Modal visible={show} transparent={true}>
      <SafeAreaView>
        <View style={styles.container}>
          <ActivityIndicator
            size="large"
            // color={Colors.primaryMain}
            color="red"
            style={{fontSize: 30}}
          />
        </View>
      </SafeAreaView>
    </Modal>
  );
};

const styles = StyleSheet.create({
   container: {
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.white,
      opacity: 0.5,
      width: '100%',
      height: '100%'
   },
})

export default Loader;
