import IcEmpty from '../assets/icon/Ic_empty';
import React from 'react';
import { View } from 'react-native';

const Icon_empty = () => {
   return (
      <View>
         <IcEmpty height={100} width={100} />
      </View>
   )
}

export default Icon_empty;