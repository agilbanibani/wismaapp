import React from 'react'
import {
   Modal,
   View,
   Text,
   Image,
   StyleSheet
} from 'react-native'
import { Mixins } from '../style'
import Colors from '../style/Colors'
import Button from './Button'

const ModalSuccess = ({show, onClose, title, subTitle, navigate}) => {
   return (
      <Modal visible={show} transparent>
         <View style={S.container}>
            <View style={S.content}>
               <Image
                  style={{width: 70, height: 70, resizeMode: 'contain'}}
                  source={require('../assets/icon/ic_transaction_success.png')}
               />
               <Text style={S.title}>{title}</Text>
               <Text style={S.subTitle}>{subTitle}</Text>
               <Button onPress={navigate} 
                  style={S.btn}>
                  <Text style={{fontSize: 18, color: Colors.white}}>Lihat Transaksi</Text>
               </Button>
            </View>
         </View>
      </Modal>
   )
}

const S = StyleSheet.create({
   container: {
      flex: 1, 
      backgroundColor: 'rgba(0,0,0,0.4)', 
      justifyContent: 'center', 
      alignItems: 'center'
   },
   content: {
      width: '90%',
      alignSelf: 'center',
      backgroundColor: '#fff',
      borderRadius: 10,
      padding: 15,
      justifyContent: 'center',
      alignItems: 'center'
   },
   title: {
      fontSize: Mixins.scaleFont(20),
      color: Colors.text_black,
      marginTop: 20
   },
   subTitle: {
      fontSize: Mixins.scaleFont(20),
      color: Colors.sub_text,
      marginTop: 16,
      textAlign: 'center'
   },
   btn: {
      marginTop: 20,
      width: '100%',
      backgroundColor: Colors.primary,
      paddingVertical: 10,
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius: 8
   }
})

export default ModalSuccess;