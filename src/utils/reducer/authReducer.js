const initialState = {
  isLoggedIn: false,
  userData: {
    nama:'Deddy Cobuzier',
    email:'a@mail.com',
    no_telp:'0852088213',
    username:'dedy',
    password:'123456',
    device_id: ''
  },
  token: false,
  listener: false
}

const authReducer = (state = initialState, action) => {
   switch (action.type) {
      case "LOGIN_SUCCESS":
        return {
          ...state,
          isLoggedIn: true,
          userData: action.login.payload
        };
      case "SET_LISTENER":
        return {
          ...state,
          listener: !action.listener
        };
      case "TOKEN":
        return {
          ...state,
          token: action.token
        };
      case "LOGOUT":
          return {
            ...state,
            isLoggedIn: false,
            userData: {
              nama:'',
              email:'',
              no_telp:'',
              username:'',
              password:'',
              device_id: ''
            },
          };
      default:
        return state;
   } 
}

export default authReducer;