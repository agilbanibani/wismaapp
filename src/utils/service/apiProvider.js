import API from './axiosConfig';
import {queryParams} from '../../helper/helper'

export default {
  Login: async (params) => {
    return API('/api/auth/login', {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
      },
      body: {
        ...params,
      },
    }).catch((err) => console.log('error', err));
  },
  Logout: async (token) => {
    return API('/api/auth/logout', {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => console.log('error', err));
  },
  Register: async (params) => {
    console.log('BODY REGISTER', params);
    return API('/api/auth/register', {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
      },
      body: {
        ...params,
      },
    }).catch((err) => {
      console.log('error register',JSON.parse(JSON.stringify(err)));
      return err;
    });
  },
  GetProductAll: async () => {
    return API('/api/product', {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Content-Type': 'application/json'
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  GetDataSlider: async () => {
    return API('/api/slider', {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Content-Type': 'application/json'
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  GetCategory: async () => {
    return API('/api/category', {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Content-Type': 'application/json'
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  GetProductFilter: async (params) => {
    return API('/api/product?' + queryParams(params), {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Content-Type': 'application/json'
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  PostCart: async (params, token) => {
    return API('/api/cart', {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: {
        ...params,
      },
    }).catch((err) => console.log('error', err));
  },
  GetAllCart: async (token) => {
    return API('/api/cart', {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  EditCartItem: async (id,params, token) => {
    return API(`/api/cart/${id}`, {
      method: 'PUT',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: {
        ...params,
      }
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  DeleteItemCart: async (id,token) => {
    return API(`/api/cart/${id}`, {
      method: 'DELETE',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  Payment: async (params, token) => {
    return API('/api/transaction', {
      method: 'POST',
      head: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: {
        ...params,
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error checkout');
      console.log('error payment', err.response);
      return undefined;
    });
  },
  GetAllHistory: async (token) => {
    return API('/api/transaction', {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  GetDetailHistory: async (id,token) => {
    return API(`/api/transaction/${id}`, {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  CancelOrder: async (params, token) => {
    return API(`/api/transaction/${params}/canceled`, {
      method: 'PUT',
      head: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error cancel order');
      return err.response;
    });
  },
  GetLogNotif: async (token) => {
    return API(`/api/notification-log`, {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  UpdateProfile: async (params, token) => {
    return API(`/api/user/update-profile`, {
      method: 'PUT',
      head: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: {
        ...params,
      }
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error update profile');
      return undefined;
    });
  },
  UpdatePassword: async (params, token) => {
    return API(`/api/user/update-password`, {
      method: 'PUT',
      head: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: {
        ...params,
      }
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error checkout');
      return err.response;
    });
  },
  GetVA: async (token) => {
    return API(`/api/payment/va-banks`, {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  GetInvoice: async (token, id) => {
    console.log('cek id invoice', id);
    return API(`/api/payment/${id}/invoice`, {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  SimulatedPayment: async (amount, id, token) => {
    return API(`https://api.xendit.co/callback_virtual_accounts/external_id=${id}/simulate_payment`, {
      method: 'POST',
      head: {
        'Accept': 'application/json',
        'Authorization': `Basic ${token}`
      },
      body: {
        ...amount,
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error checkout');
      return err.response;
    });
  },
  GetTimes: async (token) => {
    return API(`/api/times`, {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
  ActivateEmail: async (token) => {
    return API('/api/verification/resend', {
      method: 'GET',
      head: {
        "Accept": 'application/json',
        'Authorization': `Bearer ${token}`
      },
    }).catch((err) => {
      console.log(JSON.parse(JSON.stringify(err)), 'error');
      return err.response;
    });
  },
}