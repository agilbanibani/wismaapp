import React, { useState } from 'react'
import { Text, View, TouchableOpacity, StatusBar, StyleSheet, Image, ScrollView } from 'react-native'
import {useDispatch} from 'react-redux'
import {Actions} from 'react-native-router-flux'
import {useSelector} from 'react-redux'
import Colors from '../../style/Colors'
import {Icon} from 'react-native-elements'
import API from '../../utils/service/apiProvider'

const Profile = () => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const dispatch = useDispatch();
   const logout = async () => {
      Actions.reset('intro')
      const logout = await API.Logout(userData.token);
      dispatch({type: 'LOGOUT'})
   }
   
   return (
      <View style={{flex: 1}}>
         <StatusBar backgroundColor={Colors.primary} barStyle="light-content" />
         <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.header}>
               <Text style={{color: Colors.white, fontSize: 16}}>Akun Saya</Text>
            </View>
            <View style={styles.viewName}>
               <View style={{width: '70%'}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>{userData && userData.user_detail.name}</Text>
                  <Text style={{color: Colors.sub_text, fontSize: 16, marginTop: 10}}>{userData && userData.user_detail.phone_number}</Text>
               </View>
               <TouchableOpacity
                  onPress={() => Actions.editProfile()}
                  activeOpacity={0.7}
                  style={styles.btnEdit}
               >
                  <Text style={{fontSize: 15, color: Colors.primary, alignSelf: 'center', fontWeight: 'bold'}}>Edit Profile</Text>
               </TouchableOpacity>
            </View>
            <TouchableOpacity
               key="ubah_password"
               activeOpacity={0.7}
               style={styles.btnMenu}
               onPress={() => Actions.editPassword()}
            >
               <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                     style={{width: 25, height: 30, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-key.png')}
                  />
                  <Text style={{color: Colors.text_black, fontSize: 15, marginLeft: 10}}>Ubah Password</Text>
               </View>
               <Icon
                  type="antdesign"
                  name="right"
                  color={Colors.text_black}
                  size={20}
               />
            </TouchableOpacity>
            {/* <TouchableOpacity
               key="pengaturan"
               activeOpacity={0.7}
               style={styles.btnMenu}
               onPress={() => null}
            >
               <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                     style={{width: 25, height: 30, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-setiings.png')}
                  />
                  <Text style={{color: Colors.text_black, fontSize: 15, marginLeft: 10}}>Pengaturan</Text>
               </View>
               <Icon
                  type="antdesign"
                  name="right"
                  color={Colors.text_black}
                  size={20}
               />
            </TouchableOpacity> */}
            {/* <TouchableOpacity
               key="faq"
               activeOpacity={0.7}
               style={styles.btnMenu}
               onPress={() => null}
            >
               <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                     style={{width: 25, height: 30, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-faq.png')}
                  />
                  <Text style={{color: Colors.text_black, fontSize: 15, marginLeft: 10}}>FAQ</Text>
               </View>
               <Icon
                  type="antdesign"
                  name="right"
                  color={Colors.text_black}
                  size={20}
               />
            </TouchableOpacity> */}
            {/* <TouchableOpacity
               key="hub"
               activeOpacity={0.7}
               style={styles.btnMenu}
               onPress={() => null}
            >
               <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Image
                     style={{width: 25, height: 30, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-phone.png')}
                  />
                  <Text style={{color: Colors.text_black, fontSize: 15, marginLeft: 10}}>Hubungu Kami</Text>
               </View>
               <Icon
                  type="antdesign"
                  name="right"
                  color={Colors.text_black}
                  size={20}
               />
            </TouchableOpacity> */}
            <TouchableOpacity 
               onPress={() => logout()}
               style={styles.btnLogout}>
               <Image
                  style={{width: 25, height: 25, resizeMode: 'contain'}}
                  source={require('../../assets/icon/icon-logout.png')}
               />
               <Text style={{fontWeight: 'bold', fontSize: 18, color: '#E81F1F', marginLeft: 10}}>keluar</Text>
            </TouchableOpacity>
         </ScrollView>
      </View>
   )
}

const styles = StyleSheet.create({
   btnLogout: {
      paddingVertical: 20, 
      flexDirection: 'row',
      marginTop: 25, 
      justifyContent: 'center', 
      alignItems: 'center',
      borderColor: Colors.borderColor,
      borderWidth: 1
   },
   btnMenu: {
      width: '100%',
      padding: 15,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      borderBottomColor: Colors.borderColor,
      borderBottomWidth: 1
   },
   btnEdit: {
      width: '30%',
      borderRadius: 5,
      borderColor: Colors.primary,
      borderWidth: 1,
      paddingVertical: 10
   },
   viewName: {
      width: '100%',
      paddingHorizontal: 10,
      paddingVertical: 30,
      flexDirection: 'row',
      alignItems: 'center',
      borderBottomColor: Colors.borderColor,
      borderBottomWidth: 1
   },
   header: {
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 15,
      backgroundColor: Colors.primary
   }
})

export default Profile