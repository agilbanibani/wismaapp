import React, {useEffect, useState} from 'react'
import {
   View,
   Text,
   TextInput,
   TouchableOpacity,
   StyleSheet,
   Alert,
   KeyboardAvoidingView,
   ScrollView,
} from 'react-native'
import {useSelector, useDispatch} from 'react-redux'
import API from '../../utils/service/apiProvider'
import {Icon} from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import Loading from '../../components/Loader'
import Colors from '../../style/Colors'

const EditPassword = () => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const dispatch = useDispatch()
   const [loading, setLoading] = useState(false)
   const [state, setState] = useState({
      password: "",
      password_confirmation: ""
   })
   const [seePassword, setSeePassword] = useState(true)
   const [seePasswordConfirm, setSeePasswordConfirm] = useState(true)

   const updatePasword = async () => {
      setLoading(true)
      const update = await API.UpdatePassword(state, userData.token)
      if(update.success) {
         setLoading(false);
         dispatch({type: 'LOGOUT'})
         Alert.alert('Update password berhasil.', 'Silahkan login kembali.', [
            {
               text: 'Oke',
               onPress: () => Actions.reset('intro')
            }
         ])
      }
   }

   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loading show={loading} />
         <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.header}>
               <TouchableOpacity onPress={() => Actions.pop()} activeOpacity={0.7}>
                  <Icon
                     type="antdesign"
                     name="arrowleft"
                     size={30}
                     color={Colors.text_black}
                  />
               </TouchableOpacity>
            </View>
            <KeyboardAvoidingView enabled behavior="padding" keyboardVerticalOffset={-500} style={{width: '100%', paddingHorizontal: 20}}>
               <Text style={{color: Colors.primary, fontSize: 25, fontWeight: 'bold', marginTop: 15}}>Edit Password</Text>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Kata Sandi Baru</Text>
                  <View style={{width: '100%', paddingBottom: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                     <TextInput
                        placeholder="Masukkan kata sandi"
                        secureTextEntry={seePassword}
                        value={state.password}
                        onChangeText={(text) => setState({...state, password: text})}
                        style={{paddingVertical: 5, width: '90%'}}
                     />
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => setSeePassword(!seePassword)}
                           style={{padding: 5}}>
                           {
                              seePassword ? 
                              <Icon
                                 type="entypo"
                                 name="eye-with-line"
                                 size={20}
                              />
                              :
                              <Icon
                                 type="entypo"
                                 name="eye"
                                 size={20}
                              />
                           }
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Konfirmasi Kata Sandi Baru</Text>
                  <View style={{width: '100%', paddingBottom: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                     <TextInput
                        placeholder="Masukkan kata sandi"
                        secureTextEntry={seePasswordConfirm}
                        value={state.password_confirmation}
                        onChangeText={(text) => setState({...state, password_confirmation: text})}
                        style={{paddingVertical: 5, width: '90%'}}
                     />
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => setSeePasswordConfirm(!seePasswordConfirm)}
                           style={{padding: 5}}>
                           {
                              seePasswordConfirm ? 
                              <Icon
                                 type="entypo"
                                 name="eye-with-line"
                                 size={20}
                              />
                              :
                              <Icon
                                 type="entypo"
                                 name="eye"
                                 size={20}
                              />
                           }
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
            </KeyboardAvoidingView>
            <TouchableOpacity 
               onPress={() => updatePasword()}
               activeOpacity={0.7}
               style={{width: '90%', marginBottom: 20, marginTop: 60, alignSelf: 'center', paddingVertical: 15, backgroundColor: Colors.primary, borderRadius: 8}}>
               <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>Lanjutkan</Text>
            </TouchableOpacity>
         </ScrollView>
      </View>
   )
}
const styles = StyleSheet.create({
   header: {
      width: '100%',
      paddingHorizontal: 10,
      paddingVertical: 15,
      alignItems: 'flex-start'
   }
})
export default EditPassword;