import React, {useEffect, useState} from 'react'
import {
   View,
   Text,
   TextInput,
   TouchableOpacity,
   StyleSheet,
   Alert,
   KeyboardAvoidingView,
   ScrollView,
} from 'react-native'
import {Picker} from '@react-native-community/picker'
import Colors from '../../style/Colors'
import {Icon} from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import Loading from '../../components/Loader'
import DatePicker from 'react-native-datepicker'
import {useSelector, useDispatch} from 'react-redux'
import API from '../../utils/service/apiProvider'

const EditProfile = () => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const dispatch = useDispatch()
   const [loading, setLoading] = useState(false)
   const [state, setState] = useState({
      name: "",
      address: "",
      dob : "",
      gender: "M",
      phone_number: ''
   })
   const [gender, setGender] = useState([
      {
         label: 'Laki - laki',
         value: 'M'
      },
      {
         label: 'Perempuan',
         value: 'P'
      }
   ])

   const updateProfile = async() => {
      setLoading(true)
      const update = await API.UpdateProfile(state, userData.token)
      if(update) {
         if(update.success) {
            setLoading(false);
            dispatch({type: 'LOGOUT'})
            Alert.alert('Update profile berhasil.', 'Silahkan login kembali.', [
               {
                  text: 'Oke',
                  onPress: () => Actions.reset('intro')
               }
            ])
         }
      } else {
         setLoading(false);
         Alert.alert('Update profile gagal.', 'Nomor handphone telah terdaftar.', [
            {
               text: 'Oke',
               onPress: () => null
            }
         ])
      }
   }

   useEffect(() => {
      setState({
         name: userData.user_detail.name,
         address: userData.user_detail.address,
         dob: userData.user_detail.dob,
         gender: userData.user_detail.gender,
         phone_number: userData.user_detail.phone_number
      })
   },[])
   
   return(
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loading show={loading} />
         <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.header}>
               <TouchableOpacity onPress={() => Actions.pop()} activeOpacity={0.7}>
                  <Icon
                     type="antdesign"
                     name="arrowleft"
                     size={30}
                     color={Colors.text_black}
                  />
               </TouchableOpacity>
            </View>
            <KeyboardAvoidingView enabled behavior="padding" keyboardVerticalOffset={-500} style={{width: '100%', paddingHorizontal: 20}}>
               <Text style={{color: Colors.primary, fontSize: 25, fontWeight: 'bold', marginTop: 15}}>Edit Profile</Text>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Nama Lengkap</Text>
                  <TextInput
                     placeholder="Masukkan nama lengkap"
                     value={state.name}
                     onChangeText={(text) => setState({...state, name: text})}
                     style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Jenis Kelamin</Text>
                  <View style={{width: '100%', paddingBottom: 5, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}>
                     <Picker
                        selectedValue={state.gender}
                        style={{height: 50, width: '100%' }}
                        onValueChange={(itemValue, itemIndex) =>
                           setState({...state, gender: itemValue})
                        }
                     >
                        {
                           gender.map(item => {
                              return <Picker.Item label={item.label} value={item.value} />
                           })
                        }
                     </Picker>
                  </View>
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Tanggal Lahir</Text>
                  <View style={{width: '100%', flexDirection: 'row', marginTop: 10, alignItems: 'center', paddingBottom: 5, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}>
                     {/* <TouchableOpacity style={{width: '90%', alignItems: 'flex-start'}}> */}
                        <DatePicker
                           style={{width: '90%'}}
                           date={state.dob}
                           mode="date"
                           placeholder="dd/mm/yyyy"
                           format="YYYY-MM-DD"
                           minDate="1945-01-01"
                           maxDate="2018-01-01"
                           confirmBtnText="Confirm"
                           cancelBtnText="Cancel"
                           showIcon={false}
                           customStyles={{
                              dateIcon: {
                                 flex: 0,
                                 width: 0,
                                 height: 0,
                              },
                              dateInput: {
                                 borderWidth: 0,
                                 alignItems: 'flex-start',
                              }
                              // ... You can check the source to find the other keys.
                           }}
                           onDateChange={(date) => setState({...state, dob: date})}
                        />
                     {/* </TouchableOpacity> */}
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <Icon
                           type="antdesign"
                           name="calendar"
                           size={20}
                           color={Colors.text_black}
                        />
                     </View>
                  </View>
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Alamat Lengkap</Text>
                  <TextInput
                     placeholder="Masukkan alamat lengkap"
                     value={state.address}
                     onChangeText={text => setState({...state, address:text})}
                     style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Nomor Handphone</Text>
                  <TextInput
                     placeholder="Masukkan nomot handphone"
                     value={state.phone_number}
                     onChangeText={text => setState({...state, phone_number:text})}
                     style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </View>
            </KeyboardAvoidingView>
            <TouchableOpacity 
               onPress={() => updateProfile()}
               activeOpacity={0.7}
               style={{width: '90%', marginBottom: 20, marginTop: 60, alignSelf: 'center', paddingVertical: 15, backgroundColor: Colors.primary, borderRadius: 8}}>
               <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>Lanjutkan</Text>
            </TouchableOpacity>
         </ScrollView>
      </View>
   )
}
const styles = StyleSheet.create({
   header: {
      width: '100%',
      paddingHorizontal: 10,
      paddingVertical: 15,
      alignItems: 'flex-start'
   }
})
export default EditProfile;