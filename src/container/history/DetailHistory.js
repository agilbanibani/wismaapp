import React, {useState, useEffect} from 'react'
import {
   View,
   ScrollView,
   Text,
   Image,
   Alert,
   StyleSheet,
   RefreshControl,
   ToastAndroid,
   StatusBar
} from 'react-native'
import { Icon } from 'react-native-elements'
import Button from '../../components/Button'
import Colors from '../../style/Colors'
import {Mixins} from '../../style'
import API from '../../utils/service/apiProvider'
import {useSelector} from 'react-redux'
import Loading from '../../components/Loader'
import { Actions } from 'react-native-router-flux'
import moment from 'moment'
import Clipboard from '@react-native-community/clipboard';

import ModalInstruction from './component/ModalInstruction'

const DetailItem = ({navigation}) => {
   const { userData, isLoggedIn, listener } = useSelector(state => state.auth);
   const [state, setState] = useState({
      data: undefined
   })
   const [loading, setLoading] = useState(true)
   const [dataInvoice, setDataInvoice] = useState({})
   const [modalHelp, setModalHelp] = useState(false)
   const [refreshing, setRefreshing] = useState(false)
   const [copiedText, setCopiedText] = useState('');

   const getDetail = async () => {
      const detailHistory = await API.GetDetailHistory(navigation.state.params.item.id, userData.token);            
      console.log('detail history', detailHistory)
      if(detailHistory.success) {
         setState({...state, data: detailHistory.payload})
         if(detailHistory.payload.status === 'waiting_payment' && detailHistory.payload.payment_type === 'va') {
            // setLoading(false);
            getInvoice();
         } else {
            setLoading(false);
         }
      }
   }

   const getInvoice = async () => {
      const getInvoice = await API.GetInvoice(userData.token, navigation.state.params.item.id)      
      console.log('detail invoice', getInvoice)
      if(getInvoice.success) {
         // setLoading(false);
         setDataInvoice(getInvoice.payload)
         if(getInvoice.payload.status === 'ACTIVE') {
            setLoading(false);
            setDataInvoice(getInvoice.payload)
         } else {
            const regetinvoice = await API.GetInvoice(userData.token, navigation.state.params.item.id)
            if(regetinvoice.success) {
               if(regetinvoice.payload.status === 'ACTIVE') {
                  setLoading(false);
                  setDataInvoice(regetinvoice.payload)
               }
            }
         }
      }
   }

   const onCanceled = async () => {
      setLoading(true)
      const cancelOrder = await API.CancelOrder(state.data.id, userData.token)
      if(cancelOrder.success) {
         setLoading(false);
         await navigation.state.params.getData();
         Actions.pop();
      }
      // await navigation.state.params.getData;
   }

   const copyToClipBoard = () => {
      Clipboard.setString(dataInvoice.account_number);
   }

   const cek_status = (status) => {
      switch (status) {
         case 'waiting_payment':
            return (
               <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
                  <Image
                     style={{width: '20%', height: 40, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-payment-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-cooking-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-done-disable.png')}
                  />
               </View>
            )
         case 'paid':
            return (
               <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
                  <Image
                     style={{width: '20%', height: 40, tintColor: Colors.primary, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-payment-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-cooking-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-done-disable.png')}
                  />
               </View>
            )
         case 'processing':
            return (
               <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
                  <Image
                     style={{width: '20%', height: 40, tintColor: Colors.primary, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-payment-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, tintColor: Colors.primary, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-cooking-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-done-disable.png')}
                  />
               </View>
            )
         case 'canceled':
            return;
         default:
            return (
               <View style={{width: '100%', flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
                  <Image
                     style={{width: '20%', height: 40, tintColor: Colors.primary, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-payment-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, tintColor: Colors.primary, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-cooking-disable.png')}
                  />
                  <View style={{width: '20%', borderColor: Colors.greyText2, borderWidth: 1}} />
                  <Image
                     style={{width: '20%', height: 40, tintColor: Colors.primary, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icon-done-disable.png')}
                  />
               </View>
            )
      }
   }

   const statusPayment = (status) => {
      switch (status) {
         case "waiting_payment":
            return <Text style={{ color: Colors.canceled, fontSize: 16, fontWeight: 'bold'}}>Waiting Payment</Text>
         case "paid":
            return <Text style={{ color: Colors.green1, fontSize: 16, fontWeight: 'bold'}}>Paid</Text>
         case "processing":
            return <Text style={{ color: Colors.processing, fontSize: 16, fontWeight: 'bold'}}>Processing</Text>
         case "ready_pickup":
            return <Text style={{ color: Colors.green1, fontSize: 16, fontWeight: 'bold'}}>Ready Pickup</Text>
         case "deliver":
            return <Text style={{ color: Colors.green1, fontSize: 16, fontWeight: 'bold'}}>Deliver</Text>
         case "completed":
            return <Text style={{ color: Colors.green1, fontSize: 16, fontWeight: 'bold'}}>Completed</Text>
         case "cancel":
            return <Text style={{ color: Colors.canceled, fontSize: 16, fontWeight: 'bold'}}>Canceled</Text>
         default:
            break;
      }
   }

   useEffect(() => {
      getDetail();
   },[listener])
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loading show={loading} />
         <StatusBar barStyle='light-content' backgroundColor={Colors.primary} />
         <ModalInstruction
            show={modalHelp}
            onClose={() => setModalHelp(false)}
            data={dataInvoice}
            copy={() => {
               copyToClipBoard();
               ToastAndroid.showWithGravity(
                  "Code telah disalin",
                  ToastAndroid.SHORT,
                  ToastAndroid.BOTTOM
               );
            }}
         />
         {
            state.data && 
            <ScrollView 
               refreshControl={
                  <RefreshControl refreshing={refreshing} onRefresh={() => {
                     getDetail();
                  }} />
               }
               showsVerticalScrollIndicator={false} contentContainerStyle={{paddingBottom: 100}}>
               <View style={styles.header}>
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     <Button
                        onPress={() => Actions.pop()}
                     >
                        <Icon
                           type="antdesign"
                           name="arrowleft"
                           size={18}
                           color={Colors.white}
                        />
                     </Button>
                  </View>
                  <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                     <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold'}}>Pesanan #123151</Text>
                  </View>
                  <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}} />
               </View>
               {
                  state.data && cek_status(state.data.status)
               }
               <View style={{width: '100%', flexDirection: 'row', paddingBottom: 15, borderBottomColor: Colors.greyText2, borderBottomWidth: 0.4, justifyContent: 'space-between', alignItems: 'center', marginTop: 5}}>
                  <View style={{width: '30%', paddingHorizontal: 10}}>
                     <Text style={{color: Colors.greyText2, fontSize: Mixins.scaleFont(14)}}>Pembayaran Terkonfirmasi</Text>
                  </View>
                  <View style={{width: '40%', justifyContent: 'center', alignItems: 'center'}}>
                     <Text style={{color: Colors.greyText2, fontSize: Mixins.scaleFont(14)}}>Dalam Proses</Text>
                  </View>
                  <View style={{width: '30%', alignItems: 'flex-end', paddingHorizontal: 10}}>
                     <Text style={{color: Colors.greyText2, fontSize: Mixins.scaleFont(14)}}>Selesai</Text>
                  </View>
               </View>
               <View style={{width: '100%', borderBottomColor: Colors.greyText2, borderBottomWidth: 0.4, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 20}}>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                     <Text>{moment(state.data.booking_date, 'YYYY-MM-DD').format('DD MMM YYYY')}</Text>
                     <View style={{width: 5, height: 5, marginHorizontal: 8, borderRadius: 4, backgroundColor: Colors.greyText3}} />
                     <Text>{moment(state.data.time, 'YYYY-MM-DD HH:mm:ss').format('HH:mm')}</Text>
                  </View>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                     <Text style={{color: Colors.greyText2}}>Pesanan </Text>
                     <Text style={{color: Colors.greyText2}}>{state.data.code}</Text>
                  </View>
               </View>
               <View style={{width: '100%', padding: 20, borderBottomColor: Colors.greyText2, borderBottomWidth: 0.4}}>
                  <Text style={{color: Colors.greyText2, fontSize: 16}}>Detail Pesanan</Text>
                  {
                     state.data.carts.map(value => {
                        return (
                           <View style={{width: '100%', marginTop: 15, flexDirection: 'row', alignItems: 'center'}}>
                              <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                                 <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: Mixins.scaleFont(19)}}>{value.amount}x</Text>
                              </View>
                              <View style={{width: '65%', flexDirection: 'row', alignItems: 'center'}}>
                                 <View style={{width: '30%'}}>
                                    <Image
                                       style={{width: '100%', height: Mixins.scaleSize(70), borderRadius: 8}}
                                       source={{uri: value.product.cover_image}}
                                    />
                                 </View>
                                 <View style={{width: '70%', justifyContent: 'space-between', paddingHorizontal: 10}}>
                                    <Text style={{color: Colors.text_black, fontSize: Mixins.scaleFont(16), fontWeight: 'bold'}}>{value.product.name}</Text>
                                 </View>
                              </View>
                              <View style={{width: '25%', alignItems: 'flex-end'}}>
                                 <Text style={{color: Colors.text_black, fontSize: Mixins.scaleFont(15), fontWeight: 'bold'}}>{value.price}</Text>
                              </View>
                           </View>
                        )
                     })
                  }
               </View>
               <View style={{width: '100%', borderBottomColor: Colors.greyText2, borderBottomWidth: 0.4, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', padding: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>Total</Text>
                  <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>{state.data.amount}</Text>
               </View>
               <View style={{width: '100%', paddingHorizontal: 20}}>
                  <View style={{marginTop: 20}}>
                     <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>Jadwal Pemesanan</Text>
                     <Text style={{color: Colors.text_black, fontSize: 16, marginTop: 8}}>{moment(state.data.booking_date, 'YYYY-MM-DD').format('DD MM YYYY')}</Text>
                  </View>
                  <View style={{marginTop: 20}}>
                     <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>Waktu Pengambilan</Text>
                     <Text style={{color: Colors.text_black, fontSize: 16, marginTop: 8}}>{moment(state.data.time, 'YYYY-MM-DD HH:mm:ss').format('HH:mm')}</Text>
                  </View>
                  <View style={{marginTop: 20}}>
                     <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>Metode Pembayaran</Text>
                     <Text style={{color: Colors.text_black, fontSize: 16, marginTop: 8}}>{state.data.payment_type === 'va' ? 'Virtual Account' : 'E-Wallet'} {state.data.payment_detail}</Text>
                  </View>
                  <View style={{marginTop: 15, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>                     
                     {statusPayment(state.data.status)}
                     {
                        state.data.status === 'waiting_payment' &&
                        <Button onPress={() => setModalHelp(true)}
                           style={{flexDirection: 'row', marginTop: -10, alignItems: 'center'}}>
                           <Text style={{
                              color: Colors.primary, 
                              fontSize: 15
                           }}>Lihat Detail</Text>
                           <Icon
                              type="antdesign"
                              name="right"
                              size={18}
                              style={{marginLeft: 5}}
                              color={Colors.primary}
                           />
                        </Button>
                     }
                  </View>
                  {/* <Button onPress={() => simulatedPayment()} 
                     style={{width: '90%', alignSelf: 'center', borderRadius: 8, marginTop: 20, borderColor: Colors.green1, borderWidth: 1, backgroundColor: Colors.green1, paddingVertical: 10}}>
                     <Text style={{color: Colors.white, fontSize: 16, alignSelf: 'center'}}>Coba Bayar</Text>
                  </Button> */}
               </View>
            </ScrollView>
         }
         {
           state.data && state.data.status === 'waiting_payment' &&
            <Button onPress={() =>
               Alert.alert('Warning !', "Anda yakin ingin membatalkan pesanan ?",[
                  {
                     text: 'Ya',
                     onPress: () => onCanceled()
                  },
                  {
                     text: 'Tidak',
                     onPress: () => null
                  }
               ])} 
               style={{width: '90%', alignSelf: 'center', borderRadius: 8, position: 'absolute', bottom: 10, borderColor: Colors.primary, borderWidth: 1, backgroundColor: Colors.white, paddingVertical: 10}}>
               <Text style={{color: Colors.primary, fontSize: 16, alignSelf: 'center'}}>Batalkan Pesanan</Text>
            </Button>
         }
      </View>
   )
}

const styles = StyleSheet.create({
   header: {
      width: '100%',
      paddingVertical: 15,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: Colors.primary
   },
})
export default DetailItem;