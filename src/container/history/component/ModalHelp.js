import React from 'react'
import {
   Modal,
   View,
   ScrollView,
   Text,
   StatusBar,
   Image
} from 'react-native'
import Button from '../../../components/Button'
import Colors from '../../../style/Colors'

const ModalInstruction = ({
   show,
   onClose
}) => {
   return (
      <Modal visible={show}>
         <View style={{flex: 1}}>
            <View style={{width: '100%'}}>
               <Image
                  style={{width: '100%', height: 150}}
                  source={require('../../../assets/icon/BG_Orange.png')}
               />
            </View>
         </View>
      </Modal>
   )
}
export default ModalInstruction;