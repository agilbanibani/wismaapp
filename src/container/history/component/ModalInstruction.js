import React from 'react'
import {
   Modal,
   View,
   ScrollView,
   Text,
   Image,
   ImageBackground,
   TouchableOpacity,
   ToastAndroid,
   StyleSheet
} from 'react-native'
import {Icon} from 'react-native-elements'
import Button from '../../../components/Button'
import Colors from '../../../style/Colors'
import Shadow from '../../../style/Shadow'

const ModalInstruction = ({show, onClose, data, copy}) => {
   return (
      <Modal visible={show} onRequestClose={onClose}>
         <View style={{flex: 1, backgroundColor: '#f5f5f5'}}>
            <ScrollView showsVerticalScrollIndicator={false}>
               <View style={{
                  width: '100%',
                  height: 250
               }}>
                  <ImageBackground
                     resizeMode="cover"
                     style={{width: '100%', height: 250}}
                     source={require('../../../assets/icon/BG_Orange.png')}
                  >
                     <View style={{width: '100%', flexDirection: 'row', paddingTop: 20}}>
                        <Button onPress={onClose}
                           style={{width: '10%'}}>
                           <Icon
                              type="antdesign"
                              name="arrowleft"
                              color="#fff"
                              size={23}
                           />
                        </Button>
                        <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                           <Image
                              style={{width: 80, height: 100}}
                              source={require('../../../assets/icon/ic_time.png')}
                           />
                        </View>
                        <View style={{width: '10%'}} />
                     </View>
                  </ImageBackground>
               </View>
               {
                  data && data.bank_code === 'MANDIRI' &&
                  <View style={[styles.content,Shadow.shadow]}>
                     <Text style={{color: Colors.sub_text, fontSize: 16, alignSelf: 'center'}}>{data.bank_code} Virtual Account</Text>                     
                     <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', alignSelf: 'center'}}>   
                        <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: 18, marginTop: 15}}>{data.account_number}</Text>                     
                        <TouchableOpacity
                           activeOpacity={0.7}
                           onPress={copy}
                           style={{marginLeft: 10, marginTop: 30}}
                        >
                           <Image
                              style={{width: 25, height: 25, resizeMode: 'contain'}}
                              source={require('../../../assets/icon/ic_copy_new.png')}
                           />
                        </TouchableOpacity>
                     </View>
                     <View style={{width: '100%', borderColor: Colors.text_black, borderWidth: 0.2, marginTop: 20}} />
                     <Text style={{marginTop: 15, fontSize: 18, color: Colors.text_black, marginHorizontal: 15}}>Ikuti instruksi di bawah ini untuk melakukan transfer : </Text>
                     <View style={{marginTop: 15, marginHorizontal: 15, borderColor: Colors.text_black, borderWidth: 0.2, borderRadius: 5}}>
                        <View style={{padding: 10, borderBottomColor: Colors.text_black, borderBottomWidth: 0.2}}>
                           <Text style={{fontSize: 18, fontWeight: 'bold', color: Colors.text_black}}>ATM {data.bank_code}</Text>
                        </View>
                        <View style={{width: '100%', padding: 8, backgroundColor: '#bdbdbd'}}>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>1. Masukkan kartu Anda, kemudian masukkan PIN</Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>2. Pilih <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Multi Payment</Text> menu</Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>3. Masukkan <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>{data.merchant_code}</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>4. Masukkan nomor <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>{data.account_number}</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>5. Ikuti instruksi untuk menyelesaikan transaksi</Text>
                        </View>
                     </View>
                  </View>
               }
               {
                  data && data.bank_code === 'BRI' &&
                  <View style={[styles.content,Shadow.shadow]}>
                     <Text style={{color: Colors.sub_text, fontSize: 16, alignSelf: 'center'}}>{data.bank_code} Virtual Account</Text>
                     <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', alignSelf: 'center'}}>   
                        <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: 18, marginTop: 15}}>{data.account_number}</Text>                     
                        <TouchableOpacity
                           activeOpacity={0.7}
                           onPress={copy}
                           style={{marginLeft: 10, marginTop: 30}}
                        >
                           <Image
                              style={{width: 25, height: 25, resizeMode: 'contain'}}
                              source={require('../../../assets/icon/ic_copy_new.png')}
                           />
                        </TouchableOpacity>
                     </View>
                     <View style={{width: '100%', borderColor: Colors.text_black, borderWidth: 0.2, marginTop: 20}} />
                     <Text style={{marginTop: 15, fontSize: 18, color: Colors.text_black, marginHorizontal: 15}}>Ikuti instruksi di bawah ini untuk melakukan transfer : </Text>
                     <View style={{marginTop: 15, marginHorizontal: 15, borderColor: Colors.text_black, borderWidth: 0.2, borderRadius: 5}}>
                        <View style={{padding: 10, borderBottomColor: Colors.text_black, borderBottomWidth: 0.2}}>
                           <Text style={{fontSize: 18, fontWeight: 'bold', color: Colors.text_black}}>ATM {data.bank_code}</Text>
                        </View>
                        <View style={{width: '100%', padding: 8, backgroundColor: '#bdbdbd'}}>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>1. Masukkan kartu Anda, kemudian masukkan PIN</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>2. Pilih menu <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Transaksi lain</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>3. Pilih menu <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Pembayaran</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>4. Pilih menu <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Lainnya</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>5. Pilih menu <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>BRIVA</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>6. Masukkan <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>{data.account_number}</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>7. Pilih <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>YA</Text></Text>                     
                        </View>
                     </View>
                  </View>
               }
               {
                  data && data.bank_code === 'BNI' &&
                  <View style={[styles.content,Shadow.shadow]}>
                     <Text style={{color: Colors.sub_text, fontSize: 16, alignSelf: 'center'}}>{data.bank_code} Virtual Account</Text>
                     <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', alignSelf: 'center'}}>   
                        <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: 18, marginTop: 15}}>{data.account_number}</Text>                     
                        <TouchableOpacity
                           activeOpacity={0.7}
                           onPress={copy}
                           style={{marginLeft: 10, marginTop: 30}}
                        >
                           <Image
                              style={{width: 25, height: 25, resizeMode: 'contain'}}
                              source={require('../../../assets/icon/ic_copy_new.png')}
                           />
                        </TouchableOpacity>
                     </View>
                     <View style={{width: '100%', borderColor: Colors.text_black, borderWidth: 0.2, marginTop: 20}} />
                     <Text style={{marginTop: 15, fontSize: 18, color: Colors.text_black, marginHorizontal: 15}}>Ikuti instruksi di bawah ini untuk melakukan transfer : </Text>
                     <View style={{marginTop: 15, marginHorizontal: 15, borderColor: Colors.text_black, borderWidth: 0.2, borderRadius: 5}}>
                        <View style={{padding: 10, borderBottomColor: Colors.text_black, borderBottomWidth: 0.2}}>
                           <Text style={{fontSize: 18, fontWeight: 'bold', color: Colors.text_black}}>ATM {data.bank_code}</Text>
                        </View>
                        <View style={{width: '100%', padding: 8, backgroundColor: '#bdbdbd'}}>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>1. Masukkan kartu Anda, kemudian masukkan PIN</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>2. Pilih <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Menu Lain {'>'} Transfer</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>3. Pilih ke <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Rekening BNI</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>4. Masukkan <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>{data.account_number}</Text> dan klik <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Benar</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>5. Masukkan Nominal sesuai tagihan, lalu pilih <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>YA</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>6. Jangan lupa untuk memeriksa informasi yang tertera pada layar. Pastikan semua informasi dan total tagihan yang ditampilkan sudah benar. Jika benar, pilih <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>YA</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 10, lineHeight: 24, color: Colors.greyText1}}>7. Selanjutnya tekan <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>TIDAK</Text> untuk menyelesaikan transaksi.</Text>                     
                        </View>
                     </View>
                  </View>
               }
               {
                  data && data.bank_code === 'PERMATA' &&
                  <View style={[styles.content,Shadow.shadow]}>
                     <Text style={{color: Colors.sub_text, fontSize: 16, alignSelf: 'center'}}>{data.bank_code} Virtual Account</Text>
                     <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', alignSelf: 'center'}}>   
                        <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: 18, marginTop: 15}}>{data.account_number}</Text>                     
                        <TouchableOpacity
                           activeOpacity={0.7}
                           onPress={copy}
                           style={{marginLeft: 10, marginTop: 30}}
                        >
                           <Image
                              style={{width: 25, height: 25, resizeMode: 'contain'}}
                              source={require('../../../assets/icon/ic_copy_new.png')}
                           />
                        </TouchableOpacity>
                     </View>
                     <View style={{width: '100%', borderColor: Colors.text_black, borderWidth: 0.2, marginTop: 20}} />
                     <Text style={{marginTop: 15, fontSize: 18, color: Colors.text_black, marginHorizontal: 15}}>Ikuti instruksi di bawah ini untuk melakukan transfer : </Text>
                     <View style={{marginTop: 15, marginHorizontal: 15, borderColor: Colors.text_black, borderWidth: 0.2, borderRadius: 5}}>
                        <View style={{padding: 10, borderBottomColor: Colors.text_black, borderBottomWidth: 0.2}}>
                           <Text style={{fontSize: 18, fontWeight: 'bold', color: Colors.text_black}}>ATM {data.bank_code}</Text>
                        </View>
                        <View style={{width: '100%', padding: 8, backgroundColor: '#bdbdbd'}}>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>1. Masukkan kartu Anda, kemudian masukkan PIN</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>2. Pada menu utama, pilih <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Transaksi lain</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>3. Pilih Pembayaran Transfer</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>4. Pilih Pembayaran Lainnya</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>5. Pilih Pembayaran Virtual Account</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>6. Masukkan code <Text style={{fontSize: 15, color: Colors.text_black, fontWeight: 'bold'}}>{data.account_number}</Text></Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>7. Pada halaman konfirmasi, akan muncul nominal yang dibayarkan, nomor, dan nama merchant, lanjutkan jika sudah sesuai</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>8. Pilih sumber pembayaran anda dan lanjutkan</Text>
                           <Text style={{fontSize: 16, marginTop: 5, lineHeight: 24, color: Colors.greyText1}}>9. Transaksi anda selesai</Text>        
                        </View>
                     </View>
                  </View>
               }
            </ScrollView>
         </View>
      </Modal>
   )
}

const styles = StyleSheet.create({
   content: {
      width: '90%', 
      paddingVertical: 15, 
      alignSelf: 'center', 
      borderRadius: 8, 
      marginVertical: 20,
      backgroundColor: Colors.white
   }
})

export default ModalInstruction;