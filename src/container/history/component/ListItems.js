import React from 'react';
import {
   View,
   Text,
   Image,
   FlatList,
   Dimensions
} from 'react-native'
import Button from '../../../components/Button'
import Colors from '../../../style/Colors';
import {Icon} from 'react-native-elements';
import moment from 'moment'
import IconEmpty from '../../../components/Icon_empty';

const ListItems = ({
   data,
   onPress,
   refreshData,
}) => {
   return (
      <View style={{flex: 1}}>
         <FlatList
            data={data}
            refreshControl={refreshData}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item, index}) => {
               return (
                  <Button 
                     onPress={() => onPress(item)}
                     style={{
                        width: '90%',
                        alignSelf: 'center',
                        flexDirection: 'row',
                        paddingVertical: 15
                     }}>
                     <View style={{width: '15%', justifyContent: 'center', alignItems: 'center'}}>
                        <Image
                           style={{width: 25, height: 25, resizeMode: 'contain'}}
                           source={require('../../../assets/icon/Icon-invoice.png')}
                        />
                     </View>
                     <View style={{width: '75%', justifyContent: 'space-between', paddingHorizontal: 8}}>
                        <Text style={{color: Colors.text_black, fontSize: 14, fontWeight:'bold'}}>Booking Code {item.code}</Text>
                        <View style={{width: '100%', flexDirection: 'row', alignItems: 'center'}}>
                           <Text>{moment(item.booking_date, 'YYYY-MM-DD').format('DD MMM YYYY')}</Text>
                           <View style={{width: 5, height: 5, marginLeft: 2.5, borderRadius: 4, backgroundColor: Colors.greyText3}} />
                           <Text style={{marginLeft: 8}}>{moment(item.time, 'YYYY-MM-DD HH:mm:ss').format('HH:mm')}</Text>
                        </View>
                        <View style={{padding: 10, borderRadius: 5}}>
                           <Text
                              style={{
                                 color: item.status === 'completed' || item.status === 'paid' ? Colors.green1 :
                                 item.status === 'processing' ? Colors.processing : Colors.canceled,
                                 fontSize: 14
                              }}
                           >{item.status === 'waiting_payment' ? 'Waiting for payment' : item.status}</Text>
                        </View>
                     </View>
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <Icon
                           type="antdesign"
                           name="right"
                           size={18}
                           color={Colors.text_black}
                        />
                     </View>
                  </Button>
               )
            }}
            ListEmptyComponent={() => {
               return (
                  <View style={{height: Dimensions.get('window').height * 0.6, justifyContent: 'center', alignItems: 'center'}}>
                     <IconEmpty />
                     <Text style={{fontSize: 20, marginTop: 20}}>Data Pesanan Kosong.</Text>                  
                  </View>
               )
            }}
            ItemSeparatorComponent={() => (
               <View style={{width: '90%', alignSelf: 'center', borderColor: Colors.greyText2, borderWidth: 0.2}} />
            )}
         />
      </View>
   )
}

export default ListItems;