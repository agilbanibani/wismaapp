import React, {useState, useEffect} from 'react'
import {
   View,
   Text,
   RefreshControl,
   StyleSheet,
   StatusBar
} from 'react-native'
import Button from '../../components/Button'
import Colors from '../../style/Colors'
import ListItems from './component/ListItems'
import {Actions} from 'react-native-router-flux'
import Loading from '../../components/Loader'
import moment from 'moment'
import API from '../../utils/service/apiProvider'
import {useSelector} from 'react-redux'

const History = () => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const [activeIndex, setActiveIndex] = useState(0)
   const [loading, setLoading] = useState(true)
   const [dataHistory, setDataHistory] = useState([])
   const [refreshing, setRefreshing] = useState(false)

   const getAllHistory = async () => {
      const dataHistory = await API.GetAllHistory(userData.token)
      if(dataHistory.success) {
         setLoading(false)
         setDataHistory(dataHistory.payload)
      } else {
         setLoading(false)
      }
   }

   const filterData = () => {
      let finalData = []
      if(activeIndex === 0) {
         finalData = dataHistory.filter(value => {
            return value.status !== 'completed' && value.status !== 'canceled' && value.status !== 'paid'
         })
      } else {
         finalData = dataHistory.filter(value => {
            return value.status === 'paid' || value.status === 'completed'
         })
      }
      return finalData;
   }

   useEffect(() => {
      getAllHistory()
   },[])
   
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loading show={loading} />
         <StatusBar barStyle='light-content' backgroundColor={Colors.primary} />
         <View style={styles.header}>
            <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold'}}>Pesanan</Text>
         </View>
         <View style={{
            width: '100%',
            flexDirection: 'row'
         }}>
            <Button 
               onPress={() => setActiveIndex(0)}
               style={styles.btnTab}>
               <Text style={{fontSize: 16, color: Colors.white, fontWeight: 'bold', marginBottom: 10}}>Transaksi</Text>
               {
                  activeIndex === 0 && 
                  <View style={styles.halfTriangle} />
               }
            </Button>
            <Button 
               onPress={() => setActiveIndex(1)}
               style={styles.btnTab}>
               <Text style={{fontSize: 16, color: Colors.white, fontWeight: 'bold', marginBottom: 10}}>Riwayat</Text>
               {
                  activeIndex === 1 && 
                  <View style={styles.halfTriangle} />
               }
            </Button>
         </View>
         <ListItems
            data={filterData()}
            onPress={(item) => Actions.detailHistory({item, getData: () => getAllHistory()})}
            refreshData={
               <RefreshControl refreshing={refreshing} onRefresh={() => {
                  getAllHistory();
               }} />
            }
         />
      </View>
   )
}

const styles = StyleSheet.create({
   btnTab: {
      width: '50%', 
      justifyContent: 'center', 
      alignItems: 'center', 
      backgroundColor: Colors.primary, 
      paddingVertical: 15
   },
   header: {
      width: '100%',
      paddingVertical: 15,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: Colors.primary
   },
   halfTriangle: {
      width: 0,
      height: 0,
      position: 'absolute',
      bottom: 0,
      alignSelf: 'center',
      backgroundColor: 'transparent',
      borderStyle: 'solid',
      borderTopWidth: 0,
      borderRightWidth: 13,
      borderBottomWidth: 13,
      borderLeftWidth: 13,
      borderTopColor: 'transparent',
      borderRightColor: 'transparent',
      borderBottomColor: Colors.white,
      borderLeftColor: 'transparent',
   }
})
export default History;