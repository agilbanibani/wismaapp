import React, {useEffect, useState} from 'react'
import { 
   View,
   Text,
   TextInput,
   Image,
   TouchableOpacity,
   ScrollView,
   RefreshControl,
   StyleSheet
} from 'react-native'
import Colors from '../../style/Colors';
import {Icon} from 'react-native-elements'
import { Actions } from 'react-native-router-flux';
import API from '../../utils/service/apiProvider'
import Loader from '../../components/Loader'
import {NumberFormatter} from '../../helper/helper'

const DaftarMenu = ({navigation}) => {
   const [data, setData] = useState([])
   const [key, setKey] = useState('')
   const [loading, setLoading] = useState(false)
   const [refreshing, setRefreshing] = useState(false)

   const getAllProduct = async () => {
      const dataProduct = await API.GetProductAll()
      if(dataProduct) {
         setData(dataProduct.payload)
      }
   }
   
   let finalData = [];
   finalData = data.filter(value => {
      if(key) {
         return (
            value.name.toUpperCase().search(key.toUpperCase()) !== -1
         );
      } else {
         return value;
      }
   });

   useEffect(() => {
      getAllProduct()
   },[])

   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loader show={loading} />
         <ScrollView 
            refreshControl={
               <RefreshControl refreshing={refreshing} onRefresh={() => {
                  getAllProduct();
               }} />
            }
            showsVerticalScrollIndicator={false}>
            <View style={{width: '100%', paddingVertical: 20, backgroundColor: Colors.primary, paddingHorizontal: 15}}>
               <View style={{width: '100%', flexDirection: 'row', backgroundColor: Colors.white, borderRadius: 8, alignItems: 'center'}}>
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     <Icon
                        type="antdesign"
                        name="search1"
                        size={20}
                        color="#6E6E6E"
                     />
                  </View>
                  <TextInput
                     placeholder="Mau makan apa hari ini ?"
                     value={key}
                     onChangeText={(text) => setKey(text)}
                     style={{width: '90%', paddingHorizontal: 10}}
                  />
               </View>
            </View>
            <View style={{width: '100%', paddingHorizontal: 15}}>
               {
                  finalData.map(value => {
                     return (
                        <TouchableOpacity
                           key={value.name}
                           onPress={() => Actions.productDetail({item: value})}
                           activeOpacity={0.7}
                           style={{width: '100%', flexDirection: 'row', alignItems: 'center', paddingBottom: 15, borderBottomColor: Colors.grey1, borderBottomWidth: 1, marginTop: 20}}
                        >
                           <View style={{width: '20%'}}>
                              <Image
                                 source={{uri: value.cover_image}}
                                 style={{width: '100%', height: 75, borderRadius: 8}}
                              />
                           </View>
                           <View style={{width: '50%', paddingHorizontal: 10}}>
                              <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: '600'}}>{value.name}</Text>
                           </View>
                           <View style={{width: '30%', alignItems: 'flex-end'}}>
                              <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>{NumberFormatter(value.price, 'Rp ')}</Text>
                           </View>
                        </TouchableOpacity>
                     )
                  })
               }
            </View>
         </ScrollView>
      </View>
   )
}
export default DaftarMenu;