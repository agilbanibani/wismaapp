import React, {useEffect} from 'react'
import {Image, ImageBackground} from 'react-native'
import { Actions } from 'react-native-router-flux';
import {useSelector} from 'react-redux'

const Splash = () => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const cekSession = () => {
      if(isLoggedIn) {
         setTimeout(() => {
            Actions.reset('tabbar')
         }, 1500);
      } else {
         setTimeout(() => {
            Actions.reset('intro')
         }, 1500);
      }
   }
   
   useEffect(() => {
      cekSession()
   },[])
   return (
      <ImageBackground          
         source={require('../assets/icon/ic_splash.png')}
         style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      </ImageBackground>
   )
}
export default Splash;