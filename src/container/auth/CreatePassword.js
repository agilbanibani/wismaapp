import React, {useState} from 'react'
import {
   View,
   Text,
   TextInput
} from 'react-native'
import Button from '../../components/Button'
import Colors from '../../style/Colors'
import ModalSuccess from './component/ModalSuccess'
import {Icon} from 'react-native-elements'
import {Actions} from 'react-native-router-flux'

const CreatePassword = () => {
   const [modalSuccess, setModalSuccess] = useState(false)
   const [seePassword, setSeePassword] = useState(true)
   const [seePassword1, setSeePassword1] = useState(true)
   return (
      <View style={{flex: 1}}>
         <ModalSuccess
            show={modalSuccess}
            navigate={() => {
               setModalSuccess(false),
               Actions.reset('tabbar')
            }}
         />
         <View style={{padding: 10}}>
            <Text style={{color: Colors.primary, fontSize: 25, marginTop: 20, fontWeight: 'bold'}}>BUAT KATA SANDI</Text>
            <View style={{width: '100%', marginTop: 20}}>
               <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Kata Sandi</Text>
               <View style={{width: '100%', paddingBottom: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                  <TextInput
                     placeholder="Masukkan kata sandi"
                     secureTextEntry={seePassword}
                     // value={form.password}
                     // onChangeText={(text) => setForm({...form, password: text})}
                     style={{paddingVertical: 5, width: '90%'}}
                  />
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     <Button onPress={() => setSeePassword(!seePassword)}
                        style={{padding: 5}}>
                        {
                           seePassword ? 
                           <Icon
                              type="entypo"
                              name="eye-with-line"
                              size={20}
                           />
                           :
                           <Icon
                              type="entypo"
                              name="eye"
                              size={20}
                           />
                        }
                     </Button>
                  </View>
               </View>
            </View>
            <View style={{width: '100%', marginTop: 20}}>
               <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Konfirmasi Kata Sandi</Text>
               <View style={{width: '100%', paddingBottom: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                  <TextInput
                     placeholder="Masukkan kata sandi"
                     secureTextEntry={seePassword1}
                     // value={form.password}
                     // onChangeText={(text) => setForm({...form, password: text})}
                     style={{paddingVertical: 5, width: '90%'}}
                  />
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     <Button onPress={() => setSeePassword1(!seePassword1)}
                        style={{padding: 5}}>
                        {
                           seePassword1 ? 
                           <Icon
                              type="entypo"
                              name="eye-with-line"
                              size={20}
                           />
                           :
                           <Icon
                              type="entypo"
                              name="eye"
                              size={20}
                           />
                        }
                     </Button>
                  </View>
               </View>
            </View>
         </View>
         <Button
            onPress={() => setModalSuccess(true)}
            style={{
               width: '90%',
               alignSelf: 'center',
               backgroundColor: Colors.primary,
               paddingVertical: 10,
               borderRadius: 8,
               position: 'absolute',
               bottom: 10
            }}
         >
            <Text style={{fontSize: 15, color: Colors.white, fontWeight: 'bold', alignSelf: 'center'}}>Buat Kata Sandi</Text>
         </Button>
      </View>
   )
}
export default CreatePassword;