import React from 'react'
import {
   Modal,
   View,
   Text,
   TouchableOpacity,
   Image
} from 'react-native'
import {Icon} from 'react-native-elements'
import Colors from '../../../style/Colors'

const ModalSuccess = ({show, navigate}) => {
   return (
      <Modal
         visible={show}
      >  
         <View style={{flex: 1, backgroundColor: Colors.white, justifyContent: 'center', alignItems: 'center'}}>
            <Image
               style={{width: 100, height: 100, resizeMode: 'contain'}}
               source={require('../../../assets/icon/ic_success.png')}
            />
            <Text style={{color: Colors.text_black, fontSize: 20, fontWeight: 'bold', marginTop: 20}}>Registrasi Sukses!</Text>
            <Text style={{color: '#666666', fontSize: 16, marginTop: 10}}>Akun Anda siap digunakan</Text>
            <TouchableOpacity 
               onPress={navigate}
               activeOpacity={0.7}
               style={{borderRadius: 5, marginTop: 40, backgroundColor: Colors.primary, paddingVertical: 15, width: '50%'}}>
               <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>Selesai</Text>
            </TouchableOpacity>
         </View>
      </Modal>
   )
}

export default ModalSuccess;