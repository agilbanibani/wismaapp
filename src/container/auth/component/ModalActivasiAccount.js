import React from 'react';
import { Modal, View, Text, Image, TouchableOpacity } from 'react-native'
import Colors from '../../../style/Colors';

const ModalActivasiAccount = ({show, onClose, onActiveNow}) => {

   return(
      <Modal visible={show} transparent>
         <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', justifyContent: 'center', alignItems: 'center'}}>
            <View style={{width: '90%', borderRadius: 8, backgroundColor: '#fff', justifyContent: 'center', alignItems: 'center', paddingVertical: 20}}>
               <Image source={require('../../../assets/icon/lock.png')} style={{width: 80, height: 80, resizeMode: 'contain'}} />
               <View style={{width: '90%', marginTop: 20}}>
                  <Text style={{fontSize: 15, textAlign: 'center'}}>Akun anda saat ini belum aktif, silahkan aktifkan akun anda melalui email.</Text>
               </View>
               <View style={{marginTop: 20, width: '90%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', alignSelf: 'center'}}>
                  <TouchableOpacity
                     onPress={onActiveNow}
                     style={{
                        width: '48%',
                        borderRadius: 10,
                        backgroundColor: Colors.primary,
                        paddingVertical: 10,
                        paddingHorizontal: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                     }}
                  >
                     <Text style={{color: '#fff', fontSize: 13, textAlign: 'center', fontWeight: 'bold'}}>Aktifkan Sekarang</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                     onPress={onClose}
                     style={{
                        width: '48%',
                        borderRadius: 10,
                        backgroundColor: "#d50000",
                        paddingVertical: 10,
                        justifyContent: 'center',
                        alignItems: 'center'
                     }}
                  >
                     <Text style={{color: '#fff', fontSize: 13, fontWeight: 'bold'}}>Aktifkan Nanti</Text>
                  </TouchableOpacity>
               </View>
            </View>
         </View>
      </Modal>
   )

}
export default ModalActivasiAccount;