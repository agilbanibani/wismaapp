import React, { useState } from 'react'
import {View, ScrollView, KeyboardAvoidingView, Image, ToastAndroid, TextInput, TouchableOpacity, Text, Alert} from 'react-native'
import {Icon} from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import Colors from '../../style/Colors'
import API from '../../utils/service/apiProvider'
import {useDispatch} from 'react-redux'
import Loader from '../../components/Loader'
import {useSelector} from 'react-redux'
import ModalActivasi from './component/ModalActivasiAccount'

const Login = () => {
   const { token } = useSelector(state => state.auth);
   const dispatch = useDispatch()
   const [seePassword, setSeePassword] = useState(true)
   const [form, setForm] = useState({
      username: '',
      password: '',
      fcm_token: token.token

   })
   const [tokenLogin, setTokenLogin] = useState('')
   const [loading, setLoading] = useState(false)
   const [showModal, setShowModal] = useState(false)

   const onLogin = async () => {
      const dataInputEmpty = Object.keys(form).filter(data => form[data] == '')
      const dataEmpty = [...dataInputEmpty]
      
      if(dataEmpty.length > 0) {
         return Alert.alert("Warning !", `Data ${dataEmpty.join(', ')} cannot be empty.`);
      } else {
         setLoading(true)
         const login = await API.Login(form)
         if(login) {
            if(login.payload.email_verified_at === null) {
               setLoading(false);
               setShowModal(true);
               setTokenLogin(login.payload.token)
            } else {
               setLoading(false);
               dispatch({type: 'LOGIN_SUCCESS', login});
               Actions.reset('tabbar')  
            }
         } else {
            setLoading(false);
            return Alert.alert("Ooops !", "Invalid email or password.", [
               {
                  text: "OK",
                  onPress: () => null
               }
            ])
         }
      }
   }

   const activateEmail = async () => {
      setLoading(true)
      const activateEmail = await API.ActivateEmail(tokenLogin);
      if(activateEmail && activateEmail.success) {
         setLoading(false);
         ToastAndroid.showWithGravity(
            "Email aktivasi akun telah dikirimkan.",
            ToastAndroid.LONG,
            ToastAndroid.BOTTOM
         );
      }
   }
   
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>    
         <Loader show={loading} />     
         <ModalActivasi
            show={showModal}
            onClose={() => setShowModal(false)}
            onActiveNow={() => {
               setShowModal(false);
               activateEmail();
            }}
         />
         <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <KeyboardAvoidingView style={{width: '100%'}} enabled behavior='padding' keyboardVerticalOffset={-500} >
               <Image
                  style={{width: 250, height: 140, alignSelf: 'center', resizeMode: 'contain'}}
                  source={require('../../assets/icon/logo.png')}
               />
               <View style={{width: '100%', paddingHorizontal: 15}}>
                  <View style={{width: '100%', marginTop: 20}}>
                     <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Username</Text>
                     <TextInput
                        placeholder="Masukkan Username"
                        value={form.username}
                        onChangeText={(text) => setForm({...form, username: text})}
                        style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                     />
                  </View>
                  <View style={{width: '100%', marginTop: 20}}>
                     <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Kata Sandi</Text>
                     <View style={{width: '100%', paddingBottom: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <TextInput
                           placeholder="Masukkan kata sandi"
                           secureTextEntry={seePassword}
                           value={form.password}
                           onChangeText={(text) => setForm({...form, password: text})}
                           style={{paddingVertical: 5, width: '90%'}}
                        />
                        <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                           <TouchableOpacity onPress={() => setSeePassword(!seePassword)}
                              style={{padding: 5}}>
                              {
                                 seePassword ? 
                                 <Icon
                                    type="entypo"
                                    name="eye-with-line"
                                    size={20}
                                 />
                                 :
                                 <Icon
                                    type="entypo"
                                    name="eye"
                                    size={20}
                                 />
                              }
                           </TouchableOpacity>
                        </View>
                     </View>
                  </View>
                  <View style={{width: '100%', marginTop: 15, justifyContent: 'center', alignItems: 'flex-end'}}>
                     <TouchableOpacity onPress={() => Actions.forgotPassword()} activeOpacity={0.7}>
                        <Text style={{color: Colors.primary, fontWeight: 'bold', fontSize: 18}}>Lupa kata sandi ?</Text>
                     </TouchableOpacity>
                  </View>
               </View>
            </KeyboardAvoidingView>
            <TouchableOpacity 
               onPress={() => onLogin()}
               activeOpacity={0.7}
               style={{width: '90%', alignSelf: 'center', marginTop: 30, paddingVertical: 10, borderRadius: 8, backgroundColor: Colors.primary}}>
               <Text style={{color: Colors.white, fontSize: 18, fontWeight:'bold', alignSelf: 'center'}}>Masuk</Text>
            </TouchableOpacity>
         </ScrollView>
      </View>
   )
}
export default Login;