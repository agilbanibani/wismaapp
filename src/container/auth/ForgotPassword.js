import React, {useState} from 'react';
import {
   View,
   Text,
   TouchableOpacity,
   ActivityIndicator
} from 'react-native';
import {WebView} from 'react-native-webview'
import Icon from 'react-native-vector-icons/AntDesign'
import {Actions} from 'react-native-router-flux'
import Colors from '../../style/Colors';

const ForgotPassword = () => {
   const [loading, setLoading] = useState(true)
   
   return(
      <View style={{flex: 1, backgroundColor: '#fff'}}>
         <View style={{width: '100%', paddingVertical: 10, flexDirection: 'row', alignItems: 'center'}}>
            <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
               <TouchableOpacity
                  onPress={() => Actions.pop()}
                  activeOpacity={0.8}
               >
                  <Icon name="arrowleft" size={20} />
               </TouchableOpacity>
            </View>
            <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
               <Text style={{fontSize: 16, fontWeight: 'bold'}}>Lupa Password</Text>
            </View>
            <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}} />
         </View>
         <WebView
            source={{ uri: 'https://wmmugmfood.com/password/reset' }}
            onLoad={() => setLoading(false)}
            startInLoadingState={true}
            style={{flex: 1}}
            renderLoading={() => {
               return(
                  <View style={{flex: 1, justifyContent: 'flex-start', alignItems: 'center'}}>
                     <ActivityIndicator color={Colors.primary} size="large" />
                  </View>
               )
            }}
         />
      </View>
   )

}

export default ForgotPassword;