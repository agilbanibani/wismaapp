import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, StyleSheet, KeyboardAvoidingView, Image } from 'react-native'
import Colors from '../../style/Colors'
import {Icon} from 'react-native-elements'
import { Actions } from 'react-native-router-flux'

export default class OTP extends Component {
   
   render() {
      return (
         <View style={{flex: 1, backgroundColor: Colors.white}}>
            <View style={styles.header}>
               <TouchableOpacity onPress={() => Actions.pop()} activeOpacity={0.7}>
                  <Icon
                     type="antdesign"
                     name="arrowleft"
                     size={30}
                     color={Colors.text_black}
                  />
               </TouchableOpacity>
            </View>
            <View style={{width: '100%', justifyContent: 'center', alignItems: 'center'}}>
               <Image
                  style={{width: 80, height: 80, resizeMode: 'contain'}}
                  source={require('../../assets/icon/ic_verify_phone.png')}
               />
               <View style={{width: '80%', marginTop: 20}}>
                  <Text style={{color: '#292929', fontSize: 18, fontWeight: '600', textAlign: 'center', lineHeight: 20}}>Kami telah mengirimkan kode verifikasi melalui SMS ke nomor ini</Text>
               </View>
               <Text style={{color: Colors.primary, fontSize: 18, fontWeight: 'bold', marginTop: 15}}>02730237490</Text>
               <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold', marginTop: 15}}>Masukkan kode verifikasi</Text>
               <KeyboardAvoidingView enabled behavior="padding" keyboardVerticalOffset={-500} 
                  style={{width: '70%', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                  <TextInput
                     placeholder="-"
                     style={{width: '20%', textAlign: 'center', fontSize: 30, paddingBottom: 7, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
                  <TextInput
                     placeholder="-"
                     style={{width: '20%', textAlign: 'center', fontSize: 30, paddingBottom: 7, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
                  <TextInput
                     placeholder="-"
                     style={{width: '20%', textAlign: 'center', fontSize: 30, paddingBottom: 7, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
                  <TextInput
                     placeholder="-"
                     onSubmitEditing={() => Actions.createPassword()}
                     style={{width: '20%', textAlign: 'center', fontSize: 30, paddingBottom: 7, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </KeyboardAvoidingView>
               <Text style={{color: '#292929', fontSize: 18, alignSelf: 'center', marginTop: 20}}>Kirim ulang (40s)</Text>
            </View>
         </View>
      )
   }
}

const styles = StyleSheet.create({
   header: {
      width: '100%',
      paddingHorizontal: 10,
      paddingVertical: 15,
      alignItems: 'flex-start'
   }
})