import React, {useState} from 'react'
import {
   View,
   Text,
   TextInput,
   KeyboardAvoidingView,
   ScrollView,
   TouchableOpacity,
   StyleSheet,
   Alert
} from 'react-native'
import DatePicker from 'react-native-datepicker'
import {Picker} from '@react-native-community/picker'
import Colors from '../../style/Colors'
import {Icon} from 'react-native-elements'
import { Actions } from 'react-native-router-flux'
import Loading from '../../components/Loader'
import moment from 'moment'
import API from '../../utils/service/apiProvider'

const Register = () => {
   const [loading, setLoading] = useState(false)
   const [state, setState] = useState({
      name: "",
      username: "",
      email: "",
      phone_number: "",
      address: "",
      dob : "",
      gender: "M",
      password: "",
      password_confirmation: ""
   })
   const [gender, setGender] = useState([
      {
         label: 'Laki - laki',
         value: 'M'
      },
      {
         label: 'Perempuan',
         value: 'P'
      }
   ])
   const [seePassword, setSeePassword] = useState(true)
   const [seePasswordConfirm, setSeePasswordConfirm] = useState(true)

   const validateEmail = (email) => {
      if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email))
         {
            return (true)
         }
            return (false)
   }

   const onRegis = async() => {
      const {address,dob,email,gender,name,password,password_confirmation,phone_number,username} = state;
      const dataInputEmpty = Object.keys(state).filter(data => state[data] == '')
      const dataEmpty = [...dataInputEmpty]
      if(dataEmpty.length > 0) {
         return Alert.alert("Warning !", `Data ${dataEmpty.join(', ')} tidak boleh kosong.`);
      } else if(state.password !== state.password_confirmation) {
         return Alert.alert("Warning !", 'Kata Sandi dan Konfirmasi Kata Sandi anda tidak sama.');
      } else if(!/^[a-zA-Z]*$/g.test(name.replace(/\s/g, ''))) {
         return Alert.alert("Warning !", `Data nama tidak boleh ada angka.`);
      } else if(password .length < 5 && password_confirmation < 5) {
         return Alert.alert("Warning !", `Minimal panjang password adalah 5 huruf/angka.`);
      } else if(!validateEmail(email)) {
         return Alert.alert("Warning !", `Format Email anda salah.`);
      }
      else {
         setLoading(true)
         let body = {
            name: name,
            username: username,
            email: email,
            phone_number: '0'+phone_number,
            address: address,
            dob : dob,
            gender: gender,
            password: password,
            password_confirmation: password_confirmation
         }
         const doRegister = await API.Register(body);
         if(doRegister.code === 200) {
            setLoading(false)
            Alert.alert("Success !", "Berhasil mendaftar, silahkan login.", [
               {
                  text: 'Oke',
                  onPress: () => Actions.pop()
               }
            ]);
         } else {
            setLoading(false)
            Alert.alert("Warning !", "Pendaftaran akun gagal, silahkan coba kembali.", [
               {
                  text: 'Oke',
                  onPress: () => null
               }
            ]);
         }
      }
   }

   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loading show={loading} />
         <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.header}>
               <TouchableOpacity onPress={() => Actions.pop()} activeOpacity={0.7}>
                  <Icon
                     type="antdesign"
                     name="arrowleft"
                     size={30}
                     color={Colors.text_black}
                  />
               </TouchableOpacity>
            </View>
            <KeyboardAvoidingView enabled behavior="padding" keyboardVerticalOffset={-500} style={{width: '100%', paddingHorizontal: 20}}>
               <Text style={{color: Colors.primary, fontSize: 25, fontWeight: 'bold', marginTop: 15}}>REGISTRASI</Text>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Nama Lengkap</Text>
                  <TextInput
                     placeholder="Masukkan nama lengkap"
                     value={state.name}
                     onChangeText={(text) => setState({...state, name: text})}
                     style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Username</Text>
                  <TextInput
                     placeholder="Masukkan username anda"
                     value={state.username}
                     onChangeText={(text) => setState({...state, username: text})}
                     style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Email</Text>
                  <TextInput
                     placeholder="Masukkan email"
                     keyboardType='email-address'
                     value={state.email}
                     onChangeText={(text) => setState({...state, email: text})}
                     style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Nomor Handphone</Text>
                  <View style={{width: '100%', marginTop: 7, flexDirection: 'row', alignItems: 'center', paddingBottom: 5, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}>
                     <View style={{width: '15%', justifyContent: 'center', alignItems: 'flex-start', borderRightWidth: 1, borderRightColor: Colors.grey3}}>
                        <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: '600'}}>(+62)</Text>
                     </View>
                     <TextInput
                        keyboardType="number-pad"
                        placeholder="8xx xxxx xxxx"
                        maxLength={12}
                        value={state.phone_number}
                        onChangeText={(text) => setState({...state, phone_number: text})}
                        style={{width: '85%', fontSize: 18, paddingHorizontal: 10}}
                     />
                  </View>
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Jenis Kelamin</Text>
                  <View style={{width: '100%', paddingBottom: 5, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}>
                     <Picker
                        selectedValue={state.gender}
                        style={{height: 50, width: '100%' }}
                        onValueChange={(itemValue, itemIndex) =>
                           setState({...state, gender: itemValue})
                        }
                     >
                        {
                           gender.map(item => {
                              return <Picker.Item label={item.label} value={item.value} />
                           })
                        }
                     </Picker>
                  </View>
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Tanggal Lahir</Text>
                  <View style={{width: '100%', flexDirection: 'row', marginTop: 10, alignItems: 'center', paddingBottom: 5, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}>
                     {/* <TouchableOpacity style={{width: '90%', alignItems: 'flex-start'}}> */}
                        <DatePicker
                           style={{width: '90%'}}
                           date={state.dob}
                           mode="date"
                           placeholder="dd/mm/yyyy"
                           format="YYYY-MM-DD"
                           minDate="1945-01-01"
                           maxDate="2018-01-01"
                           confirmBtnText="Confirm"
                           cancelBtnText="Cancel"
                           showIcon={false}
                           customStyles={{
                              dateIcon: {
                                 flex: 0,
                                 width: 0,
                                 height: 0,
                              },
                              dateInput: {
                                 borderWidth: 0,
                                 alignItems: 'flex-start',
                              }
                              // ... You can check the source to find the other keys.
                           }}
                           onDateChange={(date) => setState({...state, dob: date})}
                        />
                     {/* </TouchableOpacity> */}
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <Icon
                           type="antdesign"
                           name="calendar"
                           size={20}
                           color={Colors.text_black}
                        />
                     </View>
                  </View>
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Alamat Lengkap</Text>
                  <TextInput
                     placeholder="Masukkan alamat lengkap"
                     value={state.address}
                     onChangeText={text => setState({...state, address:text})}
                     style={{paddingVertical: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1}}
                  />
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Kata Sandi</Text>
                  <View style={{width: '100%', paddingBottom: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                     <TextInput
                        placeholder="Masukkan kata sandi"
                        secureTextEntry={seePassword}
                        value={state.password}
                        onChangeText={(text) => setState({...state, password: text})}
                        style={{paddingVertical: 5, width: '90%'}}
                     />
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => setSeePassword(!seePassword)}
                           style={{padding: 5}}>
                           {
                              seePassword ? 
                              <Icon
                                 type="entypo"
                                 name="eye-with-line"
                                 size={20}
                              />
                              :
                              <Icon
                                 type="entypo"
                                 name="eye"
                                 size={20}
                              />
                           }
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
               <View style={{width: '100%', marginTop: 20}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Konfirmasi Kata Sandi</Text>
                  <View style={{width: '100%', paddingBottom: 10, borderBottomColor: Colors.grey3, borderBottomWidth: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                     <TextInput
                        placeholder="Masukkan kata sandi"
                        secureTextEntry={seePasswordConfirm}
                        value={state.password_confirmation}
                        onChangeText={(text) => setState({...state, password_confirmation: text})}
                        style={{paddingVertical: 5, width: '90%'}}
                     />
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => setSeePasswordConfirm(!seePasswordConfirm)}
                           style={{padding: 5}}>
                           {
                              seePasswordConfirm ? 
                              <Icon
                                 type="entypo"
                                 name="eye-with-line"
                                 size={20}
                              />
                              :
                              <Icon
                                 type="entypo"
                                 name="eye"
                                 size={20}
                              />
                           }
                        </TouchableOpacity>
                     </View>
                  </View>
               </View>
            </KeyboardAvoidingView>
            <TouchableOpacity 
               onPress={() => onRegis()}
               activeOpacity={0.7}
               style={{width: '90%', marginBottom: 20, marginTop: 60, alignSelf: 'center', paddingVertical: 15, backgroundColor: Colors.primary, borderRadius: 8}}>
               <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>Lanjutkan</Text>
            </TouchableOpacity>
         </ScrollView>
      </View>
   )
}

const styles = StyleSheet.create({
   header: {
      width: '100%',
      paddingHorizontal: 10,
      paddingVertical: 15,
      alignItems: 'flex-start'
   }
})
export default Register;