import React from 'react'
import { 
   Text, 
   View,
   Image,
   Dimensions
} from 'react-native'
import Button from '../components/Button'
import Colors from '../style/Colors'
import {Actions} from 'react-native-router-flux'

const Intro = () => {
   return (
      <View style={{flex: 1}}>
         <View style={{width: '100%', height: Dimensions.get('window').height * 0.5}}>
            <Image
               style={{width: '100%', height: '100%', resizeMode: 'cover'}}
               source={require('../assets/icon/ic_splash.png')}
            />
         </View>
         <View style={{width: '100%', height: '30%', paddingVertical: 25, justifyContent: 'space-between', alignItems: 'center'}}>
            <Button onPress={() => Actions.login()}
               style={{width: '90%', alignSelf: 'center', paddingVertical: 10, borderRadius: 8, backgroundColor: Colors.primary}}>
               <Text style={{fontSize: 15, color: '#fff', fontWeight: 'bold', alignSelf: 'center'}}>Masuk</Text>
            </Button>
            <View style={{width: '90%', alignSelf: 'center', marginVertical: 20, flexDirection: 'row', alignItems: 'center'}}>
               <View style={{width: '40%', borderColor: '#000', borderWidth: 0.3}} />
               <View style={{width: '20%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{color: Colors.grey1}}>atau</Text>
               </View>
               <View style={{width: '40%', borderColor: '#000', borderWidth: 0.3}} />
            </View>
            <Button onPress={() => Actions.reset('tabbar')}
               style={{width: '90%', alignSelf: 'center', paddingVertical: 10, borderRadius: 8, backgroundColor: Colors.white, borderColor: Colors.primary, borderWidth: 0.4}}>
               <Text style={{fontSize: 15, color: Colors.primary, fontWeight: 'bold', alignSelf: 'center'}}>Lihat Menu</Text>
            </Button>
         </View>
         <View style={{width: '100%', height: '20%', justifyContent: 'center', alignItems: 'center'}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
               <Text>Belum punya akun? </Text>
               <Button onPress={() => Actions.register()}>
                  <Text style={{fontSize: 15, color: Colors.primary, fontWeight: 'bold', alignSelf: 'center'}}>Daftar</Text>
               </Button>
            </View>
         </View>
      </View>
   )
}
export default Intro;