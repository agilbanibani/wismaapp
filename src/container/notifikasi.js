import React from 'react'
import { 
   View,
   Text,
   Image,
   TouchableOpacity,
   StyleSheet
} from 'react-native'
import Colors from '../../style/Colors';
import {Icon} from 'react-native-elements'

const History = ({navigation}) => {
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <View style={{width: '100%', paddingVertical: 15, backgroundColor: Colors.primary, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{color: Colors.white, fontSize: 18, fontWeight: 'bold'}}>Riwayat</Text>
         </View>
         <View style={{width: '100%', paddingHorizontal: 15}}>
            <View style={{width: '100%', marginTop: 15, flexDirection: 'row'}}>
               <View style={{width: '15%', justifyContent: 'center', alignItems: 'center'}}>
                  <Icon
                     type="font-awesome-5"
                     name="clipboard-list"
                     size={30}
                     color={Colors.semi_gold}
                  />
               </View>
               <View style={{width: '55%', justifyContent: 'space-between'}}>
                  <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: 18}}>Pesanan #12315</Text>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                     <Text style={{fontSize: 16, color: '#6E6E6E', fontWeight: '600'}}>23 Okt 2020</Text>
                     <View style={{width: 5, height: 5, borderRadius: 2.5, marginHorizontal: 8, backgroundColor: '#6E6E6E'}} />
                     <Text style={{fontSize: 16, color: '#6E6E6E', fontWeight: '600'}}>07:00</Text>
                  </View>
               </View>
               <View style={{width: '30%', alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                  <TouchableOpacity>
                     <Text style={{fontSize: 16, color: Colors.semi_gold, fontWeight: 'bold'}}>Lihat Detail</Text>
                  </TouchableOpacity>
               </View>
            </View>
         </View>
      </View>
   )
}
export default History;