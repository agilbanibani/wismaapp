import React, {useState, useEffect} from 'react'
import {
   View,
   ScrollView,
   TouchableOpacity,
   Text,
   Image,
   StyleSheet,
   ActivityIndicator,
   BackHandler,
   Alert,
   Dimensions
} from 'react-native';
import {Icon} from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import Colors from '../../style/Colors';
import {Mixins} from '../../style';
import Shadow from '../../style/Shadow';
import API from '../../utils/service/apiProvider';
import {useSelector} from 'react-redux';
import {NumberFormatter} from '../../helper/helper'
import DatePicker from 'react-native-datepicker'
import moment from 'moment'
import {Picker} from '@react-native-community/picker'
import Loading from '../../components/Loader'

import ModalHelp from './component/ModalHelp';
import ModalEdit from './component/ModalEdit';
import Button from '../../components/Button';
import ModalItem from './component/ItemModal';
import ModalConfirm from './component/ModalConfirmation'
import ModalSuccess from '../../components/ModalSuccess'

const Payment = ({navigation}) => {

   const now = new Date()
   const hour = now.getHours()

   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const [modalHelp, setModalHelp] = useState(false)
   const [modalEdit, setModalEdit] = useState(false)
   const [modalItem, setModalItem] = useState(false)
   const [modalConfirm, setModalConfirm] = useState(false)
   const [dataMenu, setDataMenu] = useState([])
   const [total, setTotal] = useState(0)
   const [editTotalItem, setEditTotal] = useState(0)
   const [noteEdit, setNote] = useState('')
   const [itemEdit, setItemEdit] = useState(null)
   const [AllMenu, setAllMenu] = useState([])
   const [loadingItem, setLoadingItem] = useState(false)
   const [loading, setLoading] = useState(false)
   const [state, setState] = useState({
      orderDate: moment(new Date()).format('YYYY-MM-DD'),
      pickedTime: hour+':00',
      pickedPayment: {
         name: 'MANDIRI',
         code: 'MANDIRI',
         icon: require('../../assets/icon/ic_mandiri.png')
      }
   })
   const [dataTime, setDataTime] = useState(["10:00","10:30","11:00","11:30","13:00","14:00"
      ,"15:00","16:00","17:00",,"18:00","19:00","20:00","21:00","22:00","23:00","24:00"
   ])
   const [listBank, setListBank] = useState([
      {
         "icon": require('../../assets/icon/ic_mandiri.png'),
         "name": "Mandiri",
         "code": "MANDIRI"
      },
      {
         "icon": require('../../assets/icon/ic_bni.png'),
         "name": "BNI",
         "code": "BNI"
      },
      {
         "icon": require('../../assets/icon/ic_bri.png'),
         "name": "BRI",
         "code": "BRI"
      },
      {
         "icon": require('../../assets/icon/ic_permata.png'),
         "name": "PERMATA",
         "code": "PERMATA"
      },
      {
         "icon": require('../../assets/icon/ic_ovo.png'),
         "name": "OVO",
         "code": "OVO"
      }
   ])
   const [pickedService, setPickedService] = useState('pickup')
   const [service, setService] = useState([
      {
         label: 'Diambil Sendiri',
         value: 'pickup'
      },
      {
         label: 'Diantarkan',
         value: 'delivery'
      }
   ])
   const [modalSuccess, setModalSuccess] = useState(false)

   const getCart = async () => {
      setLoadingItem(true);
      const dataCart = await API.GetAllCart(userData.token)
      if(dataCart) {
         setLoadingItem(false);
         setDataMenu(dataCart.payload.detail)
         setTotal(dataCart.payload.total)
      }
   }

   const allMenu = async () => {
      const allMenu = await API.GetProductAll();
      setAllMenu(allMenu.payload);
   }

   const addItem = async (params) => {
      const item = {
         "product_id": params.id,
         "amount": 1
      }
      const postItem = await API.PostCart(item, userData.token)
      if(postItem.code === 200) {
         getCart()
      }
   }

   const updateItem = async () => {
      if(editTotalItem === 0) {
         const deleteItem = await API.DeleteItemCart(itemEdit.id, userData.token)
         if(deleteItem.code === 200) {
            getCart()
         }
      } else {
         const body = {
            "amount": editTotalItem,
            "note": noteEdit
         }
         const updateItem = await API.EditCartItem(itemEdit.id, body, userData.token)         
         if(updateItem.code === 200) {
            getCart()
         }
      }
   }

   const checkout = async () => {
      setLoading(true)
      const body = {
         "booking_date": state.orderDate,
         "time": `${state.orderDate} ${state.pickedTime}`,
         "payment_type": state.pickedPayment.name === 'OVO' ? 'e-wallet' : "va",
         "service_type": pickedService,
         "payment_detail": state.pickedPayment.code,
      }
      const onCheckout = await API.Payment(body, userData.token)
      if(onCheckout) {
         setLoading(false)
         setModalSuccess(true)
      } else {
         setLoading(false)
         Alert.alert('Ooops..', 'Gagal melakukan pemesanan, silahkan coba lagi.', [
            {
               text: 'OK',
               onPress: () => Actions.reset('tabbar')
            }
         ])
      }
   }

   const getTime = async () => {
      const getTime = await API.GetTimes(userData.token);
      if(getTime) {
         setDataTime(getTime.payload.times)
         setState({...state, pickedTime: getTime.payload.times[0]})
      }
   }

   useEffect(() => {
      getCart();
      allMenu();
      getTime();
      const backAction = () => {
         setModalConfirm(true)
         return true;
      };
   
      const backHandler = BackHandler.addEventListener(
         "hardwareBackPress",
         backAction
      );
      return () => backHandler.remove();
   },[])
   
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loading show={loading} />
         <ModalSuccess 
            show={modalSuccess}
            title="Order Sukses"
            subTitle="Silahkan selesaikan pembayaran di transaksi."
            navigate={() => {
               setModalSuccess(false)
               Actions.jump('history')
            }}
         />
         <ModalConfirm
            show={modalConfirm}
            onClose={() => setModalConfirm(false)}
            onConfirm={() => {
               setModalConfirm(false);
               Actions.reset('tabbar')
            }}
         />
         <ModalHelp
            show={modalHelp}
            onClose={() => setModalHelp(false)}
         />
         <ModalEdit
            show={modalEdit}
            onClose={() => setModalEdit(false)}
            item={itemEdit}
            note={noteEdit}
            setNote={(text) => setNote(text)}
            totalItem={editTotalItem}
            onPlus={() => setEditTotal(editTotalItem + 1)}
            onMinus={() => setEditTotal(editTotalItem > 0 ? editTotalItem - 1 : 0)}
            onUpdate={() => {
               setModalEdit(false);
               updateItem();
            }}
         />
         <ModalItem
            data={AllMenu}
            onClose={() => setModalItem(false)}
            show={modalItem}
            pickedItem={(item) => {
               setModalItem(false);
               addItem(item);
            }}
         />
         <ScrollView contentContainerStyle={{paddingBottom: Dimensions.get('window').height * 0.2}} showsVerticalScrollIndicator={false}>
            <View style={{width: '100%', height: 250, backgroundColor: Colors.primary}}>
               <View style={styles.header}>
                  <TouchableOpacity
                     onPress={() => setModalConfirm(true)}
                     activeOpacity={0.7} 
                     style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     <Icon
                        type="antdesign"
                        name="arrowleft"
                        size={30}
                        color={Colors.white}
                     />
                  </TouchableOpacity>
                  <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                     <Text style={{fontSize: Mixins.scaleFont(20), color: Colors.white, fontWeight: 'bold'}}>Pembayaran</Text>
                  </View>
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}/>
               </View>
            </View>
            <View style={[styles.bigCard, Shadow.shadow]}>
               <Image
                  style={{width: Mixins.scaleSize(200), height: Mixins.scaleSize(140), alignSelf: 'center', resizeMode: 'contain', marginTop: 15}}
                  source={require('../../assets/icon/logo.png')}
               />
               <View style={{justifyContent: 'center', marginTop: 10, alignItems: 'center', width: '100%'}}>
                  <Text style={{color: Colors.text_black, fontSize: Mixins.scaleFont(20), fontWeight: 'bold'}}>{userData.user_detail.name}</Text>
               </View>
               <View style={{width: '100%', paddingVertical: 15}}>
                  <View style={{flexDirection: 'row', paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center'}}>
                     <Text style={{fontSize: 16, color: '#6E6E6E', fontWeight: '600'}}>Detail Pesanan</Text>
                     
                  </View>
                  {
                     loadingItem ? 
                        <View style={{width: '100%', marginTop: 15, justifyContent: 'center', alignItems: 'center'}}>
                           <ActivityIndicator
                              color="red"
                              size="large"
                           />
                        </View>
                     :
                     dataMenu.map(value => {
                        return (
                           <View key={value.id}
                              style={{width: '100%', marginTop: 15, paddingHorizontal: 15, flexDirection: 'row'}}>
                              <View style={{width: '15%', justifyContent: 'center', alignItems: 'center'}}>
                                 <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: Mixins.scaleFont(19)}}>{value.amount}x</Text>
                              </View>
                              <View style={{width: '60%', flexDirection: 'row'}}>
                                 <View style={{width: '30%'}}>
                                    <Image
                                       style={{width: '100%', height: Mixins.scaleSize(70), borderRadius: 8}}
                                       source={{uri: value.product.cover_image}}
                                    />
                                 </View>
                                 <View style={{width: '70%', justifyContent: 'space-between', paddingHorizontal: 10}}>
                                    <Text style={{color: Colors.text_black, fontSize: Mixins.scaleFont(16), fontWeight: 'bold'}}>{value.product.name}</Text>
                                    <TouchableOpacity onPress={() => {
                                       setModalEdit(true);
                                       setItemEdit(value);
                                       setNote(value.note)
                                       setEditTotal(value.amount);
                                    }}>
                                       <Text style={{color: Colors.primary, fontSize:Mixins.scaleFont(14), fontWeight: 'bold'}}>Edit</Text>
                                    </TouchableOpacity>
                                 </View>
                              </View>
                              <View style={{width: '25%', alignItems: 'flex-end'}}>
                                 <Text style={{color: Colors.text_black, fontSize: Mixins.scaleFont(15), fontWeight: 'bold'}}>{NumberFormatter(value.price, 'Rp ')}</Text>
                              </View>
                           </View>
                        )
                     })
                  }
                  <TouchableOpacity 
                     onPress={() => setModalItem(true)}
                     style={{alignSelf: 'center', marginTop: 40, flexDirection: 'row', alignItems: 'center'}}>
                     <Image
                        style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), tintColor: Colors.primary, resizeMode: 'contain'}}
                        source={require('../../assets/icon/icon-plus-rounded.png')}
                     />
                     <Text style={{fontSize: Mixins.scaleFont(16), fontWeight: 'bold', color: Colors.primary, marginLeft: 8}}>Tambah Item</Text>
                  </TouchableOpacity>
               </View>
               <View style={{width: '100%', borderColor: Colors.greyText3, borderStyle: 'dotted', borderRadius: 1, marginTop: 8, borderWidth: 1}} />
               <View style={{width: '100%', paddingHorizontal: 10, marginTop: 20}}>
                  <View style={{width: '100%', paddingVertical: 10}}>
                     <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Jadwal Pemesanan</Text>
                     <View style={{width: '100%', marginTop: 8, paddingBottom: 8, borderBottomColor: Colors.greyText2, borderBottomWidth: 0.5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>                        
                        <DatePicker
                           style={{width: '90%'}}
                           date={state.orderDate}
                           mode="date"
                           placeholder="dd/mm/yyyy"
                           format="YYYY-MM-DD"
                           minDate={moment(new Date())}
                           // maxDate="2018-01-01"
                           confirmBtnText="Confirm"
                           cancelBtnText="Cancel"
                           showIcon={false}
                           customStyles={{
                              dateIcon: {
                                 flex: 0,
                                 width: 0,
                                 height: 0,
                              },
                              dateInput: {
                                 borderWidth: 0,
                                 alignItems: 'flex-start',
                              }                              
                           }}
                           onDateChange={(date) => setState({...state, orderDate: date})}
                        />
                        <Image
                           style={{width: 25, height: 25, resizeMode: 'contain'}}
                           source={require('../../assets/icon/icon-calendar.png')}
                        />
                     </View>
                  </View>
                  <View style={{width: '100%'}}>
                     <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Waktu Pengambilan</Text>
                     <View style={{width: '100%', marginTop: 8, paddingBottom: 8, borderBottomColor: Colors.greyText2, borderBottomWidth: 0.5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <Picker
                           selectedValue={state.pickedTime}
                           style={{height: 50, width: '90%', backgroundColor: Colors.white }}
                           onValueChange={(itemValue, itemIndex) =>
                              setState({...state, pickedTime: itemValue})
                           }
                        >
                           {
                              dataTime.map(item => {
                                 return <Picker.Item label={item} value={item} />
                              })
                           }
                        </Picker>
                        <Icon
                           type="antdesign"
                           name="right"
                           size={18}
                           color={Colors.text_black}
                        />
                     </View>
                  </View>
                  <View style={{width: '100%', marginTop: 15}}>
                     <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>Jenis Pengambilan</Text>
                     <View style={{width: '100%', marginTop: 8, paddingBottom: 8, borderBottomColor: Colors.greyText2, borderBottomWidth: 0.5, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <Picker
                           selectedValue={pickedService}
                           style={{height: 50, width: '90%', backgroundColor: Colors.white }}
                           onValueChange={(itemValue, itemIndex) =>
                              setPickedService(itemValue)
                           }
                        >
                           {
                              service.map(item => {
                                 return <Picker.Item label={item.label} value={item.value} />
                              })
                           }
                        </Picker>
                        <Icon
                           type="antdesign"
                           name="right"
                           size={18}
                           color={Colors.text_black}
                        />
                     </View>
                  </View>
               </View>
               <View style={{width: '100%', paddingHorizontal: 15, marginTop: 20}}>
                  <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                     <Text style={{color: '#787878', fontSize: Mixins.scaleFont(17)}}>Pilih Metode Pembayaran</Text>
                     <Button onPress={() => setModalHelp(true)}>
                        <Image
                           style={{width: 20, height: 20, resizeMode: 'contain'}}
                           source={require('../../assets/icon/icon-information.png')}
                        />
                     </Button>
                  </View>
                  <TouchableOpacity
                     activeOpacity={1} 
                     style={[styles.btnPaymentMethode, Shadow.shadow]}>                                          
                     <View style={{width:'100%', flexDirection: 'row', alignItems: 'center'}}>
                        <View style={{width: '30%'}}>
                           <Image
                              style={{width: 90, height: 50, resizeMode: 'contain'}}
                              source={state.pickedPayment ? state.pickedPayment.icon : require('../../assets/icon/ic_mandiri.png')}
                           />
                        </View>
                        <Picker
                           selectedValue={state.pickedPayment}
                           style={{height: 50, width: '60%', backgroundColor: Colors.white }}
                           onValueChange={(itemValue, itemIndex) =>
                              setState({...state, pickedPayment: itemValue})
                           }
                        >
                           {
                              listBank.map(item => {
                                 return <Picker.Item label={item.name} value={item} />
                              })
                           }
                        </Picker>
                        <View style={{width: '10%', alignItems: 'flex-end'}}>
                           <Icon
                              type='antdesign'
                              name="right"
                              size={Mixins.scaleSize(23)}
                              color={Colors.primary}
                           />
                        </View>
                     </View>
                  </TouchableOpacity>
               </View>
            </View>
         </ScrollView>
         <View style={{width: '100%', position: 'absolute', bottom: 0, backgroundColor: Colors.white, borderTopColor: Colors.grey2, borderTopWidth: 2, paddingVertical: 15}}>
            <View style={{width: '100%', paddingHorizontal: 15, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
               <Text style={{fontSize: Mixins.scaleFont(19), fontWeight: 'bold', color: Colors.text_black}}>Total</Text>
               <Text style={{fontSize: Mixins.scaleFont(19), fontWeight: 'bold', color: Colors.text_black}}>{NumberFormatter(total, 'Rp ')}</Text>
            </View>
            <TouchableOpacity 
               activeOpacity={0.7}
               onPress={() => {
                  if(state.pickedPayment.name === 'OVO') {
                     Alert.alert('Perhatian !', 'Pastikan nomor handphone pada akun ini adalah nomor akun OVO terlebih dahulu.',[
                        {
                           text: 'Ya, Lanjutkan',
                           onPress: () => checkout()
                        },
                        {
                           text: 'Ubah nomor handphone',
                           onPress: () => Actions.jump('profile')
                        }
                     ])
                  } else {
                     checkout()
                  }
               }}
               style={{width: '90%', marginTop: 15, alignSelf: 'center', backgroundColor: Colors.primary, paddingVertical: 10, borderRadius: 10}}>
               <Text style={{color: Colors.white, fontSize: 18, fontWeight: 'bold', alignSelf: 'center'}}>Bayar Sekarang</Text>
            </TouchableOpacity>
         </View>
      </View>
   )
}
const styles = StyleSheet.create({
   bigCard: { 
      marginTop: -150,
      backgroundColor: Colors.white, 
      width: '90%', 
      alignSelf: 'center', 
      borderRadius: 8
   },
   btnPaymentMethode: {
      width: '100%',
      // paddingVertical: 5,
      marginVertical: 20,
      backgroundColor: Colors.white,
      borderRadius: 10,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
   },
   header: {
      width: '100%',
      paddingVertical: 15,
      flexDirection: 'row',
      alignItems: 'center'
   }
})
export default Payment;