import React, {useState} from 'react'
import {
   ImageBackground,
   TouchableOpacity,
   Alert,
   Text,
   TextInput,
   View,
   StyleSheet,
   ScrollView
} from 'react-native'
import Colors from '../../style/Colors';
import {Icon} from 'react-native-elements'
import {Actions} from 'react-native-router-flux'
import { NumberFormatter } from '../../helper/helper';
import {useSelector} from 'react-redux'
import API from '../../utils/service/apiProvider';

const ProductDetail = ({navigation}) => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const [total, setTotal] = useState(1)
   const data = navigation.state.params.item

   const goToPayment = async () => {
      if(!isLoggedIn) {
         Alert.alert('Warning !', 'Silahkan Login Terlebih Dahulu.');
         return
      } else {
         const item = {
            "product_id": data.id,
            "amount": total
         }
         const postItem = await API.PostCart(item, userData.token)
         
         if(postItem.success) {
            Actions.payment()
         } else {
            Alert.alert('Warning !', 'Silahkan Login Kembali ke Aplikasi.');
         }
      }
      // Actions.payment()
   }
   
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <ScrollView showsVerticalScrollIndicator={false}>
            <ImageBackground
               resizeMode="cover"
               source={{uri: data.cover_image}}
               style={{width: '100%', height: 240}}
            >
               <View style={{width: '100%', height: 240, alignItems: 'flex-start', backgroundColor:'rgba(0,0,0,0.3)'}}>
                  <TouchableOpacity onPress={() => Actions.pop()}>
                     <Icon
                        type="antdesign"
                        name="arrowleft"
                        size={30}
                        color={Colors.white}
                        style={{marginLeft: 10, marginTop: 10}}
                     />
                  </TouchableOpacity>
               </View>
            </ImageBackground>
            <View style={{width: '100%', padding: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderBottomColor: "#F0F0F0", borderBottomWidth: 4}}>
               <View style={{width: '70%'}}>
                  <Text style={{color: Colors.text_black, fontSize: 20, fontWeight: 'bold'}}>{data.name}</Text>
               </View>
               <View style={{width: '30%', alignItems: 'flex-end'}}>
                  <Text style={{color: Colors.text_black, fontSize: 20, fontWeight: 'bold'}}>{NumberFormatter(data.price, 'Rp ')}</Text>
               </View>
            </View>
            <View style={{width: '100%', paddingHorizontal: 15}}>
               <Text style={{color: Colors.text_black, fontSize: 18, marginTop: 15, fontWeight: 'bold'}}>Deskripsi</Text>
               <Text style={{color: '#6E6E6E', fontSize: 16, lineHeight: 25, marginTop: 15, fontWeight: '600', textAlign: 'left'}}>{data.description}</Text>
            </View>
            <View style={{flexDirection: 'row', marginTop: 50, alignSelf: 'center', alignItems: 'center'}}>
               <TouchableOpacity
                  onPress={() => {
                     if(total === 1) {}
                     else {setTotal(total - 1)}
                  }} 
                  style={{padding: 7, justifyContent: 'center', alignItems: 'center', borderColor: Colors.greyText2, borderWidth: 0.4, borderRadius: 5}}>
                  <Icon
                     type="antdesign"
                     name="minus"
                     size={25}
                     color={Colors.primary}
                  />
               </TouchableOpacity>
               <Text style={{color: Colors.text_black, marginHorizontal: 15, fontSize: 25, fontWeight: 'bold'}}>{total}</Text>
               <TouchableOpacity 
                  onPress={() => setTotal(total + 1)} 
                  style={{padding: 7, justifyContent: 'center', alignItems: 'center', borderColor: Colors.greyText2, borderWidth: 0.4, borderRadius: 5}}>
                  <Icon
                     type="antdesign"
                     name="plus"
                     size={25}
                     color={Colors.primary}
                  />
               </TouchableOpacity>
            </View>
         </ScrollView>
         <TouchableOpacity
            onPress={() => goToPayment()} 
            activeOpacity={0.7} style={styles.btnNext}>
            <Text style={{color: Colors.white, fontSize: 16, fontWeight: '600'}}>Lanjutkan</Text>
         </TouchableOpacity>
      </View>
   )
}
const styles = StyleSheet.create({
   btnNext: {
      width: '90%',
      position: 'absolute',
      bottom: 10,
      alignSelf: 'center',
      paddingVertical: 10,
      backgroundColor: Colors.primary,
      borderRadius: 6,
      justifyContent: 'center',
      alignItems: 'center'
   }
})
export default ProductDetail;