import React from 'react'
import {
   View,
   Text,
   Modal,
   TextInput,
   Image
} from 'react-native'
import Button from '../../../components/Button'
import Colors from '../../../style/Colors'
import {Icon} from 'react-native-elements'

const ModalEdit = ({show, onClose, item, note, setNote, totalItem, onUpdate, onMinus, onPlus}) => {
   return (
      <Modal visible={show} transparent>
         <View style={{flex: 1, paddingHorizontal: 15, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', alignItems: 'center'}}>
            <View style={{width: '100%', backgroundColor: Colors.white, borderRadius: 5, paddingVertical: 15}}>
               <View style={{width: '100%', flexDirection: 'row', paddingHorizontal: 15, justifyContent: 'space-between', alignItems: 'center'}}>
                  <Text style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>Edit Pesanan</Text>
                  <Button onPress={onClose}>
                     <Image
                        style={{width: 25, height: 25, resizeMode: 'contain', tintColor: Colors.text_black}}
                        source={require('../../../assets/icon/icon-cancel.png')}
                     />
                  </Button>
               </View>
               <Image
                  style={{width: '100%', height: 150, marginTop: 20, resizeMode: 'cover'}}
                  source={{uri: item && item.product.cover_image}}
               />
               <View style={{width: '100%', paddingHorizontal: 20, borderBottomColor: Colors.greyText2, borderBottomWidth: 0.4}}>
                  <Text style={{color: Colors.text_black, fontSize: 18}}>{item && item.product.name}</Text>
                  <TextInput
                     value={note}
                     placeholder="contoh: pedas"
                     style={{width: '100%'}}
                     onChangeText={text => setNote(text)}
                  />
               </View>
               <View style={{flexDirection: 'row', marginTop: 50, alignSelf: 'center', alignItems: 'center'}}>
                  <Button
                     onPress={() => onMinus()}
                     style={{padding: 7, justifyContent: 'center', alignItems: 'center', borderColor: Colors.greyText2, borderWidth: 0.4, borderRadius: 5}}>
                     <Icon
                        type="antdesign"
                        name="minus"
                        size={25}
                        color={Colors.primary}
                     />
                  </Button>
                  <Text style={{color: Colors.text_black, marginHorizontal: 15, fontSize: 25, fontWeight: 'bold'}}>{totalItem}</Text>
                  <Button 
                     onPress={() => onPlus()}
                     style={{padding: 7, justifyContent: 'center', alignItems: 'center', borderColor: Colors.greyText2, borderWidth: 0.4, borderRadius: 5}}>
                     <Icon
                        type="antdesign"
                        name="plus"
                        size={25}
                        color={Colors.primary}
                     />
                  </Button>
               </View>
               <Button onPress={onUpdate}
                  style={{
                     width: '90%',
                     alignSelf: 'center',
                     paddingVertical: 10,
                     backgroundColor: Colors.primary,
                     borderRadius: 8,
                     marginTop: 20
                  }}>
                  <Text style={{fontSize: 16, color: Colors.white, fontWeight: 'bold', alignSelf: 'center'}}>Perbaharui</Text>
               </Button>
            </View>
         </View>
      </Modal>
   )
}
export default ModalEdit;