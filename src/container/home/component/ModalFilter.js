import React from 'react'
import {
   Modal,
   Text,
   TouchableOpacity,
   Image,
   View,
   StyleSheet
} from 'react-native'
import {Icon} from 'react-native-elements'
import Colors from '../../../style/Colors'
import {Mixins} from '../../../style'

const ModalFilter = ({show, close, state, onApply, cancelFilter, setState}) => {
   return (
      <Modal visible={show}>
         <View style={{flex: 1, backgroundColor: '#F9FAF9'}}>
            <View style={styles.header}>
               <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                  <TouchableOpacity
                     onPress={close}
                     activeOpacity={0.7}
                  >
                     <Icon
                        type="antdesign"
                        name="close"
                        color={Colors.white}
                        size={Mixins.scaleSize(25)}
                     />
                  </TouchableOpacity>
               </View>
               <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{color: Colors.white, fontSize: Mixins.scaleFont(18), fontWeight: 'bold'}}>Filter</Text>
               </View>
               <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                  
               </View>
            </View>
            <Text style={{color: Colors.text_black, fontSize: Mixins.scaleFont(20), marginTop: 15, fontWeight: 'bold', marginLeft: 15}}>Rentang Harga</Text>
            <View style={{width: '100%', marginTop: 15, paddingHorizontal: 15}}>
               <TouchableOpacity
                  onPress={() => setState("activePrice", 0)}
                  activeOpacity={0.7} 
                  style={{width: '100%', flexDirection: 'row', backgroundColor: Colors.white, alignItems: 'center', paddingVertical: 15, borderBottomColor: Colors.grey1, borderBottomWidth: 1}}>
                  <View style={{width: '10%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                     <Icon
                        type="antdesign"
                        name="left"
                        size={Mixins.scaleSize(20)}
                        color={Colors.text_black}
                     />
                  </View>
                  <View style={{width: '80%', paddingHorizontal: 10}}>
                     <Text style={{color: '#16233E', fontSize: Mixins.scaleFont(18)}}>Rp. 16.000</Text>
                  </View>
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     {
                        state.activePrice === 0 ?
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-checked.png')}
                        />
                        :
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-unchecked.png')}
                        />
                     }
                  </View>
               </TouchableOpacity>
               <TouchableOpacity
                  onPress={() => setState("activePrice", 1)}
                  activeOpacity={0.7} 
                  style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: Colors.white, alignItems: 'center', paddingVertical: 15, borderBottomColor: Colors.grey1, borderBottomWidth: 1}}>
                  <View style={{width: '90%', paddingHorizontal: 10}}>
                     <Text style={{color: '#16233E', fontSize: Mixins.scaleFont(18)}}>Rp. 16.000 sampai Rp. 40.000</Text>
                  </View>
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     {
                        state.activePrice === 1 ?
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-checked.png')}
                        />
                        :
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-unchecked.png')}
                        />
                     }
                  </View>
               </TouchableOpacity>
               <TouchableOpacity
                  onPress={() => setState("activePrice", 2)}
                  activeOpacity={0.7} 
                  style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', backgroundColor: Colors.white, alignItems: 'center', paddingVertical: 15, borderBottomColor: Colors.grey1, borderBottomWidth: 1}}>                  
                  <View style={{width: '90%', paddingHorizontal: 10}}>
                     <Text style={{color: '#16233E', fontSize: Mixins.scaleFont(18)}}>Rp. 40.000 sampai Rp. 100.000</Text>
                  </View>
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     {
                        state.activePrice === 2 ?
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-checked.png')}
                        />
                        :
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-unchecked.png')}
                        />
                     }
                  </View>
               </TouchableOpacity>
               <TouchableOpacity
                  onPress={() => setState("activePrice", 3)}
                  activeOpacity={0.7} 
                  style={{width: '100%', flexDirection: 'row', backgroundColor: Colors.white, alignItems: 'center', paddingVertical: 15, borderBottomColor: Colors.grey1, borderBottomWidth: 1}}>
                  <View style={{width: '10%', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                     <Icon
                        type="antdesign"
                        name="right"
                        size={Mixins.scaleSize(20)}
                        color={Colors.text_black}
                     />
                  </View>
                  <View style={{width: '80%', paddingHorizontal: 10}}>
                     <Text style={{color: '#16233E', fontSize: Mixins.scaleFont(18)}}>Rp. 100.000</Text>
                  </View>
                  <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                     {
                        state.activePrice === 3 ?
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-checked.png')}
                        />
                        :
                        <Image
                           style={{width: Mixins.scaleSize(30), height: Mixins.scaleSize(30), resizeMode: 'contain'}}
                           source={require('../../../assets/icon/icon-unchecked.png')}
                        />
                     }
                  </View>
               </TouchableOpacity>
            </View>
            <View style={{position: 'absolute', bottom: 10, width: '100%'}}>
               <TouchableOpacity 
                  activeOpacity={0.7}
                  onPress={onApply}
                  style={{width: '90%', alignSelf: 'center', paddingVertical: 15, borderRadius: 8, backgroundColor: Colors.primary}}>
                  <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>Terapkan</Text>
               </TouchableOpacity>
               <TouchableOpacity 
                  activeOpacity={0.7}
                  onPress={cancelFilter}
                  style={{width: '90%', alignSelf: 'center', marginTop: 15, paddingVertical: 15, borderRadius: 8, borderColor: Colors.primary, borderWidth: 1}}>
                  <Text style={{color: Colors.primary, fontSize: 16, fontWeight: 'bold', alignSelf: 'center'}}>Batalkan</Text>
               </TouchableOpacity>
            </View>
         </View>
      </Modal>
   )
}
const styles = StyleSheet.create({
   header: {
      width: '100%',
      paddingVertical: 15,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: Colors.primary
   }
})
export default ModalFilter;