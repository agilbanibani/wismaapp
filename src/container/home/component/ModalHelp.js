import React from 'react'
import {
   View,
   Text,
   Modal
} from 'react-native'
import Button from '../../../components/Button'
import Colors from '../../../style/Colors'

const ModalHelp = ({show, onClose}) => {
   return (
      <Modal visible={show} transparent>
         <View style={{flex: 1, paddingHorizontal: 15, backgroundColor: 'rgba(0,0,0,0.5)', justifyContent: 'center', alignItems: 'center'}}>
            <View style={{width: '100%', backgroundColor: Colors.white, borderRadius: 5, padding: 15}}>
               <Text style={{fontSize: 18, color: Colors.text_black, fontWeight: 'bold'}}>Kamu bisa memilih metode pembayaran yang tersedia di bawah ini: </Text>
               <View style={{width: '100%', marginTop: 10, flexDirection: 'row'}}>
                  <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>1.</Text>
                  <View style={{paddingLeft: 5, paddingTop: 2}}>
                     <Text style={{fontSize: 14, color: Colors.text_black}}>BCA Virtual Account</Text>
                     <View style={{flexDirection: 'row', marginTop: 8, alignItems: 'center'}}>
                        <View style={{width: 10, height: 10, borderRadius: 5, backgroundColor: Colors.text_black}} />
                        <Text style={{fontSize: 14, marginLeft: 8, color: Colors.text_black}}>Mandiri Virtual Account</Text>
                     </View>
                     <View style={{flexDirection: 'row', marginTop: 8, alignItems: 'center'}}>
                        <View style={{width: 10, height: 10, borderRadius: 5, backgroundColor: Colors.text_black}} />
                        <Text style={{fontSize: 14, marginLeft: 8, color: Colors.text_black}}>Bank Lainnya</Text>
                     </View>
                  </View>
               </View>
               <View style={{width: '100%', flexDirection: 'row', marginTop: 5}}>
                  <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>2.</Text>
                  <View style={{paddingLeft: 5, paddingTop: 2}}>
                     <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 14, color: Colors.text_black}}>Kartu Kredit</Text>
                     </View>
                     <View style={{flexDirection: 'row', marginTop: 8, alignItems: 'center'}}>
                        <View style={{width: 10, height: 10, borderRadius: 5, backgroundColor: Colors.text_black}} />
                        <Text style={{fontSize: 14, marginLeft: 8, color: Colors.text_black}}>Visa</Text>
                     </View>
                     <View style={{flexDirection: 'row', marginTop: 8, alignItems: 'center'}}>
                        <View style={{width: 10, height: 10, borderRadius: 5, backgroundColor: Colors.text_black}} />
                        <Text style={{fontSize: 14, marginLeft: 8, color: Colors.text_black}}>Master Card</Text>
                     </View>
                  </View>
               </View>
               <View style={{width: '100%', flexDirection: 'row', marginTop: 5}}>
                  <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>3.</Text>
                  <View style={{paddingLeft: 5, paddingTop: 2}}>
                     <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{fontSize: 14, color: Colors.text_black}}>e-Wallet</Text>
                     </View>
                     <View style={{flexDirection: 'row', marginTop: 8, alignItems: 'center'}}>
                        <View style={{width: 10, height: 10, borderRadius: 5, backgroundColor: Colors.text_black}} />
                        <Text style={{fontSize: 14, marginLeft: 8, color: Colors.text_black}}>OVO</Text>
                     </View>
                  </View>
               </View>
               <Button onPress={onClose}
                  style={{
                     width: '100%',
                     paddingVertical: 10,
                     backgroundColor: Colors.primary,
                     borderRadius: 8,
                     marginTop: 20
                  }}>
                  <Text style={{fontSize: 16, color: Colors.white, fontWeight: 'bold', alignSelf: 'center'}}>Tutup</Text>
               </Button>
            </View>
         </View>
      </Modal>
   )
}
export default ModalHelp;