import React from 'react'
import {
   Modal,
   View,
   Text,
   TouchableOpacity,
   ActivityIndicator,
   FlatList
} from 'react-native'
import Colors from '../../../style/Colors'
import {Mixins} from '../../../style'
import {Icon, Image} from 'react-native-elements'
import {NumberFormatter} from '../../../helper/helper'

const ModalItem = ({
   show,
   onClose,
   data,
   pickedItem
}) => {
   return (
      <Modal visible={show} transparent>
         <View style={{flex: 1, backgroundColor: 'rgba(0,0,0,0.4)', padding: 10}}>
            <View style={{width: '100%', maxHeight: '100%', paddingHorizontal: 10, backgroundColor: Colors.white, borderRadius: 8}}>
               <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 10}}>
                  <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>Daftar Menu</Text>
                  <TouchableOpacity
                     activeOpacity={0.7}
                     onPress={onClose}
                  >
                     <Icon
                        type="antdesign"
                        name="close"
                        size={18}
                     />
                  </TouchableOpacity>
               </View>
               <FlatList
                  data={data}
                  keyExtractor={(item, index) => index.toString()}
                  showsVerticalScrollIndicator={false}
                  renderItem={({item, index}) => {
                     return(
                        <TouchableOpacity
                           activeOpacity={0.7}
                           onPress={() => pickedItem(item)}
                           style={{width: '100%', flexDirection: 'row', alignItems: 'center', paddingBottom: 15, borderBottomColor: Colors.grey1, borderBottomWidth: 1, marginTop: 10}}
                        >
                           <View style={{width: '20%'}}>
                              <Image
                                 source={{uri: item.cover_image}}
                                 style={{width: '100%', height: 75, borderRadius: 8}}
                                 PlaceholderContent={
                                    <View style={{width: 60, height: 60, borderRadius: 50, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center'}}>
                                       <ActivityIndicator color='red' />
                                    </View>
                                 }
                              />
                           </View>
                           <View style={{width: '50%', paddingHorizontal: 10}}>
                              <Text style={{fontSize: Mixins.scaleFont(16), color: Colors.text_black, fontWeight: '600'}}>{item.name}</Text>
                           </View>
                           <View style={{width: '30%', alignItems: 'flex-end'}}>
                              <Text style={{fontSize: Mixins.scaleFont(16), color: Colors.text_black, fontWeight: 'bold'}}>{NumberFormatter(item.price, 'Rp ')}</Text>
                           </View>
                        </TouchableOpacity>
                     )
                  }}
               />
            </View>
         </View>
      </Modal>
   )
}
export default ModalItem;