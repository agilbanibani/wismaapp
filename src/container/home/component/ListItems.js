import React from 'react'
import {
   View,
   FlatList,
   TouchableOpacity,
   ImageBackground,
   Text,
   ActivityIndicator,
   StyleSheet
} from 'react-native'
import Colors from '../../../style/Colors';
import Shadow from '../../../style/Shadow'
import {NumberFormatter} from '../../../helper/helper'

const ListItems = ({data, loading, onPress, title, seeAllPress}) => {
   if(loading) {
      return (
         <View style={{width: '100%', height: 150, justifyContent: 'center', alignItems: 'center'}}>
            <ActivityIndicator color="red" size="large" />
         </View>
      )
   } else {
      return (
         <View style={{width: '100%'}}>
            <View style={{width: '100%', marginTop: 25, paddingHorizontal: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
               <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: 20}}>{title}</Text>
               <TouchableOpacity onPress={seeAllPress}>
                  <Text style={{color: Colors.primary, fontSize: 16, fontWeight: '600'}}>Lihat Semua</Text>
               </TouchableOpacity>
            </View>
            <FlatList
               showsHorizontalScrollIndicator={false}
               data={data}
               horizontal
               keyExtractor={(item, index) => index.toString()}
               renderItem={({item, index}) => {
                  return (
                     <TouchableOpacity 
                        onPress={() => onPress(item)}
                        style={[styles.card, Shadow.shadow, {
                           marginLeft: index === 0 ? 10 : 15,
                           marginRight: index === data.length - 1 ? 10 : 0
                        }]}
                     >
                        <ImageBackground
                           resizeMode="cover"
                           imageStyle={{borderTopLeftRadius: 8, borderTopRightRadius: 8}}
                           style={{width: 145, height: 145}}
                           source={{uri: item.cover_image}}
                        />
                        <View style={{paddingHorizontal: 10}}>
                           <Text numberOfLines={2} ellipsizeMode='tail' style={{color: Colors.text_black, fontSize: 18, fontWeight: 'bold'}}>{item.name}</Text>                           
                        </View>
                        <Text style={{marginTop: 20, position:'absolute', bottom: 10, left: 10, fontSize: 16, fontWeight: '600', color: Colors.text_black}}>{NumberFormatter(item.price, 'Rp')}</Text>
                     </TouchableOpacity>
                  )
               }}
            />
         </View>
      )
   }
}
const styles = StyleSheet.create({
   card: {
      width: 145,
      height: 250,
      marginBottom: 10,
      marginTop: 20,
      paddingBottom: 15,
      borderRadius: 8,
      backgroundColor: '#fff'
   }
})
export default ListItems;