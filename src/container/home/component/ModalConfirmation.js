import React from 'react'
import {
   Modal,
   View,
   Text,
   TouchableOpacity
} from 'react-native'
import {Icon} from 'react-native-elements'
import Colors from '../../../style/Colors'

const ModalConfirmation = ({
   show,
   onClose,
   onConfirm
}) => {
   return (
      <Modal visible={show} transparent>
         <View style={{flex: 1, paddingHorizontal: 10, backgroundColor: 'rgba(0,0,0,0.4)', justifyContent: 'center', alignItems: 'center'}}>
            <View style={{width: '100%', backgroundColor: Colors.white, borderRadius: 5, paddingVertical: 10}}>
               <View style={{width: '100%', paddingHorizontal: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', paddingVertical: 15, borderBottomColor: Colors.borderColor, borderBottomWidth: 1}}>
                  <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>Batalkan Pesanan</Text>
                  <TouchableOpacity
                     activeOpacity={0.7}
                     onPress={onClose}
                  >
                     <Icon
                        type="antdesign"
                        name="close"
                        size={18}
                        color={Colors.text_black}
                     />
                  </TouchableOpacity>
               </View>
               <View style={{width: '100%', paddingHorizontal: 10}}>
                  <Text style={{color: Colors.text_black, marginTop: 20, fontSize: 18, fontWeight: 'bold', textAlign: 'center'}}>Kamu yakin ingin kembali ke halaman depan ?</Text>
                  <Text style={{color: Colors.sub_text, marginTop: 20, fontSize: 16, textAlign: 'center'}}>Tenang, pesananmu sudah kami simpan dan akan ditambahkan dengan menu yang baru.</Text>
                  <TouchableOpacity
                     onPress={onConfirm}
                     activeOpacity={0.7}
                     style={{width: '100%', marginTop: 20, paddingVertical: 10, borderRadius: 8, backgroundColor: Colors.primary}}
                  >
                     <Text style={{color: Colors.white, fontSize: 18, fontWeight: 'bold', alignSelf: 'center'}}>OK</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                     onPress={onClose}
                     activeOpacity={0.7}
                     style={{width: '100%', marginTop: 10, paddingVertical: 10, borderRadius: 8, backgroundColor: Colors.white, borderColor: Colors.primary, borderWidth: 1}}
                  >
                     <Text style={{color: Colors.primary, fontSize: 18, fontWeight: 'bold', alignSelf: 'center'}}>Batal</Text>
                  </TouchableOpacity>
               </View>
            </View>
         </View>
      </Modal>
   )
}
export default ModalConfirmation;