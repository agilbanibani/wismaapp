import React, {useState, useEffect} from 'react'
import {
   ScrollView,
   View,
   Text,
   TouchableOpacity,
   FlatList,
   Image,
   ActivityIndicator,
   StyleSheet,
   Dimensions
} from 'react-native'
import Colors from '../../style/Colors';
import {Icon} from 'react-native-elements'
import { Actions } from 'react-native-router-flux';
import {NumberFormatter} from '../../helper/helper'
import {Image as Images} from 'react-native-elements'
import Loader from '../../components/Loader'

import ModalFilter from './component/ModalFilter'
import API from '../../utils/service/apiProvider';
import { Mixins } from '../../style';

const AllMenu = ({navigation}) => {
   let [dataProduct, setData] = useState([])
   const [dummydata, setDummy] = useState([
      {
         name: 'Bubur Ayam',
         image: require('../../assets/img/menu-5.png'),
         price: '12.000'
      },
      {
         name: 'Nasi Telor',
         image: require('../../assets/img/menu-5.png'),
         price: '10.000'
      },
      {
         name: 'Nasi Ayam',
         image: require('../../assets/img/menu-5.png'),
         price: '14.000'
      },
   ])
   const [openFilter, setOpenFilter] = useState(false)
   const [loading, setLoading] = useState(true)
   const [state, setState] = useState({
      activePrice: 0,
      openFilter: false
   })

   const searchProduct = async () => {
      var start_price = 0;
      var end_price = 0;
      if(state.activePrice == 1) {
         start_price = 16000;
         end_price = 40000;
      } else if(state.activePrice == 2) {
         start_price = 40000;
         end_price = 100000;
      } else if(state.activePrice == 3) {
         start_price = 100000;
         end_price = 1000000;
      } else {
         start_price = 0;
         end_price = 16000;
      }
      
      let params = {
         start_price: start_price,
         end_price: end_price
      }
      const dataFilter = await API.GetProductFilter(params);
      if(dataFilter) {
         setData(dataFilter.payload)
      }
   }

   const getAllProduct = async () => {
      const allProduct = await API.GetProductAll();
      if(allProduct) {
         setLoading(false)
         setData(allProduct.payload)
      }
   }

   const getProductId = async () => {
      const params = {
         category_id: navigation.state.params.id
      }
      const dataProduct = await API.GetProductFilter(params);
      if(dataProduct) {
         setLoading(false)
         setData(dataProduct.payload)
      }
   }

   useEffect(() => {
      if(navigation.state.params && navigation.state.params.data) {
         setData(navigation.state.params.data);
         setLoading(false)
      } else {
         getProductId()
      }
   },[])
   
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loader show={loading} />
         <ModalFilter
            show={state.openFilter}
            close={() => setState({...state, openFilter: false})}
            state={state}
            cancelFilter={() =>{
               if(navigation.state.params && navigation.state.params.id) {
                  getProductId()
               } else {
                  getAllProduct()
               }
               setState({...state, openFilter: false}),
               setLoading(true)
            }}
            onApply={() => {
               searchProduct(),
               setState({...state, openFilter: false})
            }}
            setState={(key, value) => setState({...state, [key]: value})}
         />
         <View style={styles.header}>
            <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
               <TouchableOpacity
                  onPress={() => Actions.pop()}
                  activeOpacity={0.7}
               >
                  <Icon
                     type="antdesign"
                     name="arrowleft"
                     color={Colors.white}
                     size={25}
                  />
               </TouchableOpacity>
            </View>
            <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
               <Text style={{color: Colors.white, fontSize: 18, fontWeight: 'bold'}}>{navigation.state.params.title}</Text>
            </View>
            <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
               <TouchableOpacity
                  // onPres={() => setState({...state, openFilter: true})}
                  onPress={() =>setState({...state, openFilter: true})}
                  activeOpacity={0.7}
               >
                  <Image
                     style={{width: 30, height: 30, resizeMode: 'contain'}}
                     source={require('../../assets/icon/icn-filter-white.png')}
                  />
               </TouchableOpacity>
            </View>
         </View>
         <View style={{width: '100%', paddingHorizontal: 15, paddingBottom: 100}}>
            <FlatList
               data={dataProduct}
               showsVerticalScrollIndicator={false}
               nestedScrollEnabled
               ListEmptyComponent={() => {
                  return (
                     <View style={{width: '100%', height: Dimensions.get('window').height * 0.8, justifyContent: 'center', alignItems: 'center'}}>
                        <Text style={{color: Colors.text_black, fontSize: Mixins.scaleFont(15)}}>Data Produk dengan harga yang anda cari tidak ada.</Text>
                     </View>
                  )
               }}
               keyExtractor={(item, index) => index.toString()}
               renderItem={({item, index}) => {
                  return (
                     <TouchableOpacity
                        key={item.name}
                        onPress={() => Actions.productDetail({item: item})}
                        activeOpacity={0.7}
                        style={{width: '100%', flexDirection: 'row', alignItems: 'center', paddingBottom: 15, borderBottomColor: Colors.grey1, borderBottomWidth: 1, marginTop: 20}}
                     >
                        <View style={{width: '20%'}}>
                           <Images
                              source={{uri: item.cover_image}}
                              style={{width: '100%', height: 75, borderRadius: 8}}
                              PlaceholderContent={
                                 <View style={{width: 60, height: 60, borderRadius: 50, backgroundColor: 'transparent', justifyContent: 'center', alignItems: 'center'}}>
                                    <ActivityIndicator color='red' />
                                 </View>
                              }
                           />
                        </View>
                        <View style={{width: '50%', paddingHorizontal: 10}}>
                           <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: '600'}}>{item.name}</Text>
                        </View>
                        <View style={{width: '30%', alignItems: 'flex-end'}}>
                           <Text style={{fontSize: 16, color: Colors.text_black, fontWeight: 'bold'}}>{NumberFormatter(item.price, 'Rp ')}</Text>
                        </View>
                     </TouchableOpacity>
                  )
               }}
            />
         </View>
      </View>
   )
}
const styles = StyleSheet.create({
   header: {
      width: '100%',
      paddingVertical: 15,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: Colors.primary
   }
})
export default AllMenu;