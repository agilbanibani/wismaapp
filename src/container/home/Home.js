import React, {useState, useEffect} from 'react'
import {
   View,
   ScrollView,
   Text,
   TouchableOpacity,
   StyleSheet,
   TextInput,
   Image,
   ImageBackground,
   BackHandler,
   RefreshControl,
   ActivityIndicator
} from 'react-native'
import Colors from '../../style/Colors'
import {Icon} from 'react-native-elements'
import {Actions} from 'react-native-router-flux'
import Shadow from '../../style/Shadow'
import API from '../../utils/service/apiProvider'
import {useSelector} from 'react-redux'
import moment from 'moment'
import "moment/locale/id"

import Slider from '../../components/Slider'
import ListItems from './component/ListItems'

const Home = ({navigation, route}) => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);

   const [activeDotIndex, setActiveDot] = useState(0);
   const [dataProduct, setDataProduct] = useState([]);
   const [dataSlider, setDataSlider] = useState([]);
   const [dataCategory, setDataCategory] = useState([]);
   const [loading, setLoading] = useState(true);
   const [loadingCategory, setLoadingCategory] = useState(true);
   const [loadingSlider, setLoadingSlider] = useState(true);
   const [refreshing, setRefreshing] = useState(false)
   const today = moment(new Date())

   const getHumanTime = () => {
		const now = new Date()
		const hour = now.getHours()
		switch (true) {
		  case hour < 12:
			return "Selamat Pagi";
		  case hour < 15:
			return "Selamat Siang";
		  case hour < 18:
			return "Selamat Sore";
		  default:
			return "Selamat Malam";
		}
   }

   const getAllProduct = async () => {
      const dataProduct = await API.GetProductAll()
      if(dataProduct) {
         setLoading(false)
         setDataProduct(dataProduct.payload)
      }
   }

   const getSlider = async () => {
      const dataSlider = await API.GetDataSlider();
      if(dataSlider) {
         setLoadingSlider(false)
         setDataSlider(dataSlider.payload)
      }
   }

   const getCategory = async () => {
      const dataCategory = await API.GetCategory()
      if(dataCategory) {
         setLoadingCategory(false);
         setDataCategory(dataCategory.payload)
      }
   }

   useEffect(() => {
      getAllProduct();
      getSlider();
      getCategory();
      const backAction = () => {
         if (navigation.isFocused()) {
            BackHandler.exitApp()
            return true;
         }
      };
   
      const backHandler = BackHandler.addEventListener(
         "hardwareBackPress",
         backAction
      );
      return () => backHandler.remove();
   },[])
   
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <ScrollView 
            refreshControl={
               <RefreshControl refreshing={refreshing} onRefresh={() => {
                  getAllProduct();
                  getSlider();
                  getCategory();
               }} />
             }
            showsVerticalScrollIndicator={false}>
            <ImageBackground 
               source={require('../../assets/icon/BG_Orange.png')}
               style={{width: '100%', paddingBottom: 10}}>
               <View style={{width: '100%', paddingHorizontal: 15, paddingTop: 20}}>
                  <View style={{width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                     {
                        isLoggedIn ? 
                        <Text style={{color: Colors.white, fontWeight: 'bold', fontSize: 20}}>{isLoggedIn ? `${getHumanTime()} ${userData.username}`: `${getHumanTime()}`}</Text>
                        :
                        <TouchableOpacity 
                           activeOpacity={0.7}
                           onPress={() => Actions.login()}
                        >
                           <Text style={{color: Colors.white, fontWeight: 'bold', fontSize: 20}}>Login Disini</Text>
                        </TouchableOpacity>
                     }
                     <TouchableOpacity
                        activeOpacity={0.7}
                        onPress={() => Actions.notificationScreen()}
                     >
                        <Image
                           style={{width: 20, height: 20, resizeMode: 'contain', tintColor: Colors.white}}
                           source={require('../../assets/icon/ic_empty_notification.png')}
                        />
                     </TouchableOpacity>
                  </View>
                  <TouchableOpacity 
                     onPress={() => Actions.menu()}
                     activeOpacity={0.7}
                     style={{width: '100%', marginTop: 20, flexDirection: 'row', alignItems: 'center', backgroundColor: Colors.white, borderRadius: 8}}>
                     <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
                        <Icon
                           type="antdesign"
                           name="search1"
                           size={20}
                           color="#6E6E6E"
                        />
                     </View>
                     <TextInput
                        placeholder="Mau makan apa hari ini?"
                        placeholderTextColor="#6E6E6E"
                        style={{width: '80%'}}
                        editable={false}
                     />
                  </TouchableOpacity>
               </View>
               <Slider 
                  data={dataSlider}
                  loading={loadingSlider}
                  setState={(index) => setActiveDot(index)} 
                  activeDotIndex={activeDotIndex}
                  onPress={(item) => Actions.promodetail({item}) }
               />
            </ImageBackground>
            <ListItems
               data={dataProduct}
               onPress={(value) => Actions.productDetail({item: value})}
               seeAllPress={() => Actions.allmenu({title: 'Rekomendasi Untukmu', data: dataProduct})}
               title="Rekomendasi Untukmu"
               loading={loading}
            />
            {
               loadingCategory ?
               <View style={{width: '100%', paddingVertical: 20, justifyContent: 'center', alignItems: 'center'}}>
                  <ActivityIndicator color="red" size="large" />
               </View>
               :
               <View style={{width: '100%', paddingHorizontal: 15, marginVertical: 20, paddingBottom: 25, borderBottomColor: Colors.grey1, borderBottomWidth: 1}}>
                  <Text style={{color: Colors.text_black, fontWeight: 'bold', fontSize: 20}}>Kategori</Text>
                  <View style={{marginTop: 15, width: '100%', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                     {
                        dataCategory.map(value => {
                           return (
                              <TouchableOpacity 
                                 key={value.id}
                                 activeOpacity={0.7}
                                 onPress={() => Actions.allmenu({title: value.name, id: value.id})}
                                 style={[styles.cardKategory, Shadow.shadow]}>
                                 <Image
                                    style={{width: 60, height: 80, resizeMode: 'contain'}}
                                    source={{uri: value.cover_image}}
                                 />
                                 <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: '600'}}>{value.name}</Text>
                              </TouchableOpacity>
                           )
                        })
                     }
                  </View>
               </View>
            }
            {/* <ListItems
               data={[0,1,2]}
               onPress={() => null}
               seeAllPress={() => null}
               title="Menu Lainnya"
            /> */}
         </ScrollView>
      </View>
   )
}

const styles = StyleSheet.create({
   cardKategory: {
      width: '31%', 
      borderRadius: 8, 
      backgroundColor: Colors.white, 
      justifyContent: 'center', 
      alignItems: 'center', 
      paddingVertical: 15
   }
})
export default Home;