import React, {useState, useEffect} from 'react'
import {
   View,
   Text,
   TouchableOpacity,
   FlatList,
   Image,
   StyleSheet,
   Dimensions
} from 'react-native'
import moment from 'moment'
import Colors from '../../style/Colors'
import {Actions} from 'react-native-router-flux'
import {Icon} from 'react-native-elements'
import { Mixins } from '../../style'
import {useSelector} from 'react-redux'
import API from '../../utils/service/apiProvider'
import Loading from '../../components/Loader'

const NotificationScreen = () => {
   const { userData, isLoggedIn } = useSelector(state => state.auth);
   const [loading, setLoading] = useState(false)
   const [history, setHistory] = useState([])

   const icons = (status) => {
      switch (status) {
         case 0:
            return require('../../assets/icon/icon-money.png');
         case 1:
            return require('../../assets/icon/icon-cook.png');
         case 2:
            return require('../../assets/icon/icon-flag.png');
         default:
            return require('../../assets/icon/icon-money.png');
      }
   }

   const getLogNotification = async () => {
      setLoading(true)
      const lognotif = await API.GetLogNotif(userData.token)
      if(lognotif.success) {
         setLoading(false)
         setHistory(lognotif.payload)
      }
   }

   useEffect(() => {
      getLogNotification()
   },[])

   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <Loading show={loading} />
         <View style={styles.header}>
            <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}}>
               <TouchableOpacity onPress={() => Actions.pop()}>
                  <Icon
                     type="antdesign"
                     name="arrowleft"
                     size={20}
                     color={Colors.white}
                  />
               </TouchableOpacity>
            </View>
            <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
               <Text style={{color: Colors.white, fontSize: 18}}>Notifikasi</Text>
            </View>
            <View style={{width: '10%', justifyContent: 'center', alignItems: 'center'}} />
         </View>
         <FlatList
            data={history}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({item}) => {
               return(
                  <TouchableOpacity 
                     activeOpacity={0.7}
                     style={styles.item}>
                     <View style={{width: '15%', alignItems: 'center', justifyContent: 'flex-start'}}>
                        <Image
                           style={{width: 35, height: 35, resizeMode: 'contain'}}
                           source={require('../../assets/icon/ic_notification.png')}
                        />
                     </View>
                     <View style={{width: '85%', paddingHorizontal: 10, paddingBottom: 10, borderBottomColor: Colors.borderColor, borderBottomWidth: 1}}>
                        <Text style={{fontSize: Mixins.scaleFont(16), fontWeight: 'bold', color: Colors.text_black}}>{item.title}</Text>
                        <Text style={{fontSize: Mixins.scaleFont(14), color: Colors.sub_text}}>{item.body}</Text>
                        {/* <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
                           <Text style={{color: Colors.greyText2}}>23 Okt 2020</Text>
                           <View style={{width: 5, height: 5, marginLeft: 2.5, borderRadius: 4, backgroundColor: Colors.greyText3}} />
                           <Text style={{marginLeft: 8,color: Colors.greyText2}}>07:00</Text>
                        </View> */}
                     </View>
                  </TouchableOpacity>
               )
            }}
            ListEmptyComponent={() => {
               return (
                  <View style={{flex: 1, height: Dimensions.get('window').height, justifyContent: 'center', alignItems: 'center'}}>
                     <Text style={{fontSize: 16}}>Notifikasi kosong</Text>
                  </View>
               )
            }}
         />
      </View>
   )
}

const styles = StyleSheet.create({
   item: {
      width: '100%',
      paddingVertical: 20,
      // paddingHorizontal: 15,
      flexDirection: 'row'
   },
   header: {
      width: '100%',
      paddingVertical: 15,
      backgroundColor: Colors.primary,
      flexDirection :'row',
      alignItems: 'center'
   }
})
export default NotificationScreen;