import React from 'react'
import {
   View,
   Text,
   Image,
   ScrollView,
   TouchableOpacity,
   StyleSheet
} from 'react-native'
import Colors from '../../style/Colors';
import {Icon} from 'react-native-elements'
import {Actions} from 'react-native-router-flux'

const PromoDetail = ({navigation}) => {
   const data = navigation.state.params.item
   return (
      <View style={{flex: 1, backgroundColor: Colors.white}}>
         <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.header}>
               <TouchableOpacity onPress={() => Actions.pop()}
                  style={{width: '10%', justifyContent:'center', alignItems: 'center'}}>
                  <Icon
                     type="antdesign"
                     name="arrowleft"
                     size={20}
                     color={Colors.white}
                  />
               </TouchableOpacity>
               <View style={{width: '80%', justifyContent: 'center', alignItems: 'center'}}>
                  <Text style={{color: Colors.white, fontSize: 16, fontWeight: 'bold'}}>Detail Promo</Text>
               </View>
               <View style={{width: '10%'}} />
            </View>
            <View style={{width: '100%', paddingBottom: 10}}>
               <Image
                  source={{uri: data.image}}
                  style={{width: '100%', height: 300, resizeMode: 'stretch'}}
               />
               <View style={{marginTop: 15, width: '100%', paddingHorizontal: 15}}>
                  <Text style={{color: Colors.text_black, fontSize: 16, fontWeight: 'bold'}}>{data.name}</Text>
                  <Text style={{color: Colors.text_black, fontSize: 15, lineHeight: 23, marginTop: 10}}>{data.description}</Text>
               </View>
            </View>
         </ScrollView>
      </View>
   )
}

const styles = StyleSheet.create({
   header: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: Colors.primary,
      paddingVertical: 15
   }
})

export default PromoDetail;