const Colors = {

  //primary colour
  primary: '#ff5722',
  warning: '#E88D32',

  //white colour
  white: '#FFFFFF',
  black: '#000',
  text_black: '#003B40',
  sub_text: '#999999',
  semi_gold: '#90761C',
  borderColor: '#E1E1E1',

  //grey colour 1 -> 5 = dark -> light
  greyText1: '#6E6F8D',
  greyText2: '#919191',
  greyText3: '#9EA1AD',
  greyBackground: '#F1F3F6',
  grey1: '#D7D7E5',
  grey2: '#D0D1E3',
  grey3: '#212121',
  grey4: '#616161',

  //green
  green1: '#12BC93',
  green2: '#00c853',
  canceled: '#d32f2f',
  processing: '#ffc107'
 
 };
 
 export default Colors;
 