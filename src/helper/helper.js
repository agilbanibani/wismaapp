export const queryParams = (obj) => {
   var str = [];
   for (var p in obj) {
      if (obj.hasOwnProperty(p)) {
         str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
   }
   return str.join('&');
};

export const NumberFormatter = (nominal, currency) => {
   let rupiah = '';
   const nominalref = nominal.toString().split('').reverse().join('');
   for (let i = 0; i < nominalref.length; i++) {
      if (i % 3 === 0) {
         rupiah += nominalref.substr(i, 3) + '.';
      }
   }

   if (currency) {
      currency = currency.replace(/\s/g, ' ');
      return (
         currency +
         rupiah
         .split('', rupiah.length - 1)
         .reverse()
         .join('')
      );
   } else {
      return rupiah
         .split('', rupiah.length - 1)
         .reverse()
         .join('');
   }
};

export const ucfirst = (s) => {
   if (typeof s !== 'string') {
      return '';
   }
   return s.charAt(0).toUpperCase() + s.slice(1);
};

export const convertColor = (colour, value) => {
   const opacity = Math.floor(0.1 * value * 255).toString(16);
   return colour + opacity;
};
