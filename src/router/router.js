import React from 'react';
import {Image} from 'react-native'
import {Router, Scene, } from 'react-native-router-flux';
import { Icon } from 'react-native-elements';
import {connect} from 'react-redux'

import Colors from '../style/Colors'

import Home from '../container/home/Home'
import Profile from '../container/profile/Profile'
import ProductDetail from '../container/home/ProductDetail'
import Payment from '../container/home/Payment'
import DaftarMenu from '../container/menu/DaftarMenu'
import Splash from '../container/Splash'
import Login from '../container/auth/Login'
import Register from '../container/auth/Register'
import OTP from '../container/auth/OTP'
import AllMenu from '../container/home/AllMenu'
import Intro from '../container/Intro'
import CreatePassword from '../container/auth/CreatePassword'
import History from '../container/history/History'
import DetailHistory from '../container/history/DetailHistory'
import NotificationScreen from '../container/home/NotificationScreen'
import EditProfile from '../container/profile/EditProfile'
import EditPassword from '../container/profile/UpdatePassword';
import PromoDetail from '../container/home/PromoDetail'
import ForgotPassword from '../container/auth/ForgotPassword'

const TabIcon = ({type, iconName, focused}) => {
   switch (iconName) {
      case 'home':
         return (
            <Image 
               style={{width: 25, height: 25, resizeMode: 'contain', tintColor: focused ? Colors.primary: Colors.grey1}}
               source={require('../assets/icon/home.png')}
            />
         )
      case 'daftar':
         return (
            <Image 
               style={{width: 25, height: 25, resizeMode: 'contain', tintColor: focused ? Colors.primary: Colors.grey1}}
               source={require('../assets/icon/menu.png')}
            />
         )
      case 'pesanan':
         return (
            <Image 
               style={{width: 25, height: 25, resizeMode: 'contain', tintColor: focused ? Colors.primary: Colors.grey1}}
               source={require('../assets/icon/pesanan.png')}
            />
         )
      default:
         return (
            // <Icon name={iconName} type={type ? `${type}` : "font-awesome"} iconStyle={{ color: focused ? Colors.primary: Colors.grey1 }}/>
            <Image 
               style={{width: 25, height: 25, resizeMode: 'contain', tintColor: focused ? Colors.primary: Colors.grey1}}
               source={require('../assets/icon/profile.png')}
            />
         )
   }
};

class Routing extends React.Component {
   
   render() {      
      return (
         <Router>
            <Scene key="root">
               <Scene key="splash" component={Splash} hideNavBar initial />
               <Scene key="tabbar" tabs showLabel={true} inactiveTintColor={Colors.grey1} activeTintColor={Colors.primary}
                  hideNavBar>
                     <Scene key="home" component={Home} title="Beranda" iconName="home" icon={TabIcon} hideNavBar />      			
                     <Scene key="menu" component={DaftarMenu} title="Daftar Menu" hideNavBar iconName="daftar" icon={TabIcon}/>
                     <Scene key="history" component={History} title="Pesanan" type="pesanan" hideNavBar iconName="pesanan" icon={TabIcon}/>
                     {
                        this.props.auth.isLoggedIn &&
                        <Scene key="profile" component={Profile} title="Akun Saya" iconName="user" icon={TabIcon} hideNavBar />
                     }
               </Scene>
               <Scene key="profile" component={Profile} hideNavBar />
               <Scene key="history" component={History} hideNavBar />
               <Scene key="productDetail" component={ProductDetail} hideNavBar />
               <Scene key="login" component={Login} hideNavBar />
               <Scene key="register" component={Register} hideNavBar />
               <Scene key="otp" component={OTP} hideNavBar />
               <Scene key="payment" component={Payment} hideNavBar />
               <Scene key="allmenu" component={AllMenu} hideNavBar />
               <Scene key="intro" component={Intro} hideNavBar />
               <Scene key="createPassword" component={CreatePassword} hideNavBar />
               <Scene key="detailHistory" component={DetailHistory} hideNavBar />
               <Scene key="notificationScreen" component={NotificationScreen} hideNavBar />
               <Scene key="editProfile" component={EditProfile} hideNavBar />
               <Scene key="editPassword" component={EditPassword} hideNavBar />
               <Scene key="promodetail" component={PromoDetail} hideNavBar />
               <Scene key="forgotPassword" component={ForgotPassword} hideNavBar />
            </Scene>
         </Router>
      );
   }
 }
 
 const mapStateToProps = ({auth}) => ({
   auth,
 });
 
 export default connect(mapStateToProps)(Routing);