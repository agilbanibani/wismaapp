import PushNotification from 'react-native-push-notification'
export const LocalNotification = (notification) => {
   PushNotification.localNotification({
      largeIcon: "ic_launcher", // (optional) default: "ic_launcher". Use "" for no large icon.
      smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher". Use "" for default small icon.
      color: "red", // (optional) default: system default
      vibrate: true, // (optional) default: true
      vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
      priority: "high", // (optional) set notification priority, default: high
      ignoreInForeground: false, // (optional) if true, the notification will not be visible when the app is in the foreground (useful for parity with how iOS notifications appear)
      title: notification.title, // (optional)
      message: notification.message, // (required)
      visibility: 'public'
    });
}